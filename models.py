# -*- coding: utf-8 -*-

from django.db import models
from django.core.validators import MinLengthValidator
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.translation import ugettext_lazy as _
from django.utils.datetime_safe import datetime

from syjon import settings
from apps.syjon.lib.validators import validate_white_space
import math
import syjon
import datetime

def semesters_list():
    """
    Returns the list with numbers of all posible semesters.
    """
    return range(1,11)
semester_minimal = semesters_list()[0]
semester_maximal = semesters_list()[len(semesters_list())-1]

#########################################
#           MANAGERS
#########################################

def active_manager(klass):
    def active(self):
        return self.get_query_set().filter(is_active = True)
    klass.active = active
    return klass

def didactic_offer_manager(klass):
    def didactic_offer(self):
        return self.get_query_set().filter(didactic_offer__is_active = True, is_active = True)
    klass.didactic_offer = didactic_offer
    return klass

def didactic_offer_and_future_manager(klass):
    def didactic_offer_and_future(self):
        offer = DidacticOffer.objects.get(is_active=True)
        return self.get_query_set().filter(didactic_offer__start_date__gte = offer.start_date, is_active = True)
    klass.didactic_offer_and_future = didactic_offer_and_future
    return klass

def future_manager(klass):
    def future(self):
        offer = DidacticOffer.objects.get(is_active=True)
        return self.get_query_set().filter(didactic_offer__start_date__gt = offer.start_date, is_active = True)
    klass.future = future
    return klass
    
def latest_manager(klass):
    def latest(self):
        return self.get_query_set().filter(is_last = True, is_active = True)
    klass.latest = latest
    return klass


#########################################
#           ABSTRACTS
#########################################

class AbstractUniqueName(models.Model):
    class Meta:
        abstract = True
        ordering = ('name',)
        verbose_name = _(u'Name')
        verbose_name_plural = _(u'Names')

    name = models.CharField(max_length = 256,
                            unique = True,
                            verbose_name = _(u'name'),
                            validators = [MinLengthValidator(2), validate_white_space])

    def __unicode__(self):
        return unicode(self.name)

    def save(self, *args, **kwargs):
        self.name = self.name.strip()
        super(AbstractUniqueName, self).save(*args, **kwargs)

class AbstractName(models.Model):
    class Meta:
        abstract = True
        ordering = ('name',)
        verbose_name = _(u'Name')
        verbose_name_plural = _(u'Names')

    name = models.CharField(max_length = 256,
                            verbose_name = _(u'name'),
                            validators = [MinLengthValidator(2), validate_white_space])

    def __unicode__(self):
        return unicode(self.name)

    def save(self, *args, **kwargs):
        self.name = self.name.strip()
        super(AbstractName, self).save(*args, **kwargs)

class AbstractActive(AbstractName):
    class Meta:
        abstract = True
        ordering = ('-is_active', 'name')
        verbose_name = _(u'Name, active')
        verbose_name_plural = _(u'Names, active')
        
    is_active = models.BooleanField(default = True,
                                    verbose_name = _(u'Active'))

    def __unicode__(self):
        return '%s, %s' % (unicode(self.name), unicode(self.is_active))

    def set_active(self, active):
        self.is_active = active
        self.save()

class AbstractDidacticOffer(AbstractActive):
    class Meta:
        abstract = True
        ordering = ('-is_active', 'name')
        verbose_name = _(u'Teaching offer record')
        verbose_name_plural = _(u'Teaching offer records')

    didactic_offer = models.ForeignKey('DidacticOffer',
                                       db_column = 'id_didactic_offer',
                                       null = True,
                                       blank = True,
                                       verbose_name = _(u'Teaching offer'))

    def __unicode__(self):
        return unicode(self.name)

    def get_start_date(self):
        raise NotImplementedError

    def get_end_date(self):
        raise NotImplementedError

    def get_prev(self):
        raise NotImplementedError

    def get_next(self):
        raise NotImplementedError

    def is_course_first(self):
        raise NotImplementedError

    def is_course_last(self):
        raise NotImplementedError

    def _find_offer_for_date(self, offers, date):
        for offer in offers:
            if date >= offer.start_date and date <= offer.end_date:
                return offer
        return None

    def date(self, date, sem = 0, begin = True):
        """
        Returns the date of current didactic offer after sem semesters.
        It will be the beginning or endind date of current didactic offer but with replaced year.
        
        It is used to calculate ie. the start and end date of course. 
        """
        
        def find_active_offer(offers):
            for offer in offers:
                if offer.is_active:
                    return offer
            return None
        
        if not date:
            return None
        
        if sem < 0:
            sem = 0
        
        didactic_offers = DidacticOffer.objects.order_by('start_date')
        i = 0
        offers_cnt = len(didactic_offers)
        while i < offers_cnt:
            offer = didactic_offers[i]
            if date >= offer.start_date and date <= offer.end_date:
                break
            i += 1
        
        i += sem
        if i >= offers_cnt:
            return None
        
        offer = didactic_offers[i]
        if begin:
            return offer.start_date
        else:
            return offer.end_date

    def diff(self, date0, date1):
        """
        Return the number of semesters between dates date0 and date1.
        """
        offers = DidacticOffer.objects.order_by('start_date')

        offer0 = self._find_offer_for_date(offers, date0)
        offer1 = self._find_offer_for_date(offers, date1)
        
        if not offer0 or not offer1:
            return None
        
        if date0 < date1:
            start_offer = offer0
            end_offer = offer1
        else:
            start_offer = offer1
            end_offer = offer0
        
        semester = 0
        for offer in offers:
            if offer == end_offer:
                break
            elif offer == start_offer:
                semester = 1
            elif semester > 0:
                semester += 1
        return semester 

#########################################
#           SETTINGS
#########################################

class MerovingianSettings(models.Model):
    class Meta:
        db_table = 'merv_settings'
        verbose_name = _(u'Merowing Settings')
        verbose_name_plural = _(u'Merowing Settings')

    key = models.CharField(db_column = 'key',
                           max_length = 32,
                           unique = True,
                           verbose_name = _(u'key'))
    value = models.CharField(max_length = 128,
                             verbose_name = _(u'value'))

    def __unicode__(self):
        return '%s: %s' % (unicode(self.key), unicode(self.value))

class MerovingianAdmin(models.Model):
    class Meta:
        db_table = 'merv_admin'
        verbose_name = _(u'Merowing Administrator')
        verbose_name_plural = _(u'Merowing Administrator')

    user_profile = models.OneToOneField('trainman.UserProfile',
                                        db_column = 'merv_admin__to__user_profile',
                                        verbose_name = _(u'User'))
    
    temporary_privileged_access = models.BooleanField(default = False,
                                                      verbose_name = _(u'Temporary Privileged Access'))
    courses = models.ManyToManyField('Course',
                                    db_table = 'merv_admin__to__course',
                                    null = True,
                                    blank = True,
                                    verbose_name = _(u'Managed courses'))

    def __unicode__(self):
        return unicode(self.user_profile)

class MerovingianNews(AbstractActive):
    class Meta:
        db_table = 'merv_news'
        ordering = ('-date', '-is_active')
        verbose_name = _(u'News')
        verbose_name_plural = _(u'News')
    
    date = models.DateField(db_column = 'date',
                            verbose_name = _(u'date'))
    text = models.TextField(verbose_name = _(u'content'))

#########################################
#           DIDACTIC OFFER
#########################################

class DidacticOffer(AbstractActive):
    class Meta:
        db_table = 'merv_didactic_offer'
        verbose_name = _(u'Teaching offer')
        verbose_name_plural = _(u'Teaching offers')

    start_date = models.DateField(db_column = 'start_date',
                                  verbose_name = _(u'start date'))
    end_date = models.DateField(db_column = 'end_date',
                                verbose_name = _(u'end date'))

    def __unicode__(self):
        return '%s, %s, %s - %s' % (unicode(self.name), unicode(self.is_active), unicode(self.start_date), unicode(self.end_date))

    def save(self, *args, **kwargs):
        if self.is_active:
            for d in DidacticOffer.objects.filter(is_active = True):
                d.set_active(False)
        super(DidacticOffer, self).save(*args, **kwargs)


#########################################
#               COURSES
#########################################

LEVEL_BA = 8
LEVEL_MSC = 9
LEVEL_PHD = 10
LEVEL_U_MSC = 11
LEVEL_ENG = 12
LEVEL_PG_Q = 13 # Q - Qualifying
LEVEL_PG_FL = 14 # FL - Final learning

class CourseLevel(AbstractUniqueName):
    class Meta:
        db_table = 'merv_course_level'
        verbose_name = _(u'Education level')
        verbose_name_plural = _(u'Education levels')

class CourseType(AbstractUniqueName):
    class Meta:
        db_table = 'merv_course_type'
        verbose_name = _(u'Course type')
        verbose_name_plural = _(u'Course types')

class CourseProfile(AbstractUniqueName):
    class Meta:
        db_table = 'merv_course_profile'
        verbose_name = _(u'Education profile')
        verbose_name_plural = _(u'Education profiles')



@active_manager
@didactic_offer_manager
@didactic_offer_and_future_manager
@future_manager
@latest_manager
class CourseManager(models.Manager):
    
    def active(self):
        raise NotImplementedError('decorated')
    
    def didactic_offer(self):
        raise NotImplementedError('decorated')
    
    def latest(self):
        raise NotImplementedError('decorated')
    
    def didactic_offer_and_future(self):
        raise NotImplementedError('decorated')
    
    def future(self):
        raise NotImplementedError('decorated')
    
    def active_between_dates(self, from_date, to_date):
        return self.active().filter(start_date__lte=to_date, end_date__gte=from_date)
    
class Course(AbstractDidacticOffer):
    class Meta:
        db_table = 'merv_course'
        ordering = ('-is_active', 'name', '-level__name', '-type__name', 'profile__name', 'start_date')
        verbose_name = _(u'Course')
        verbose_name_plural = _(u'Courses')
        permissions = (
            ('assign_education_area_course', 'Can assign education area'),
        )

    semesters = models.IntegerField(db_column = 'semesters',
                                    max_length = 2,
                                    null = True,
                                    blank = True,
                                    verbose_name = _(u'Number of semesters'))
    years = models.IntegerField(db_column = 'years',
                                    max_length = 2,
                                    null = True,
                                    blank = True,
                                    verbose_name = _(u'Number of years'))
    start_date = models.DateField(db_column = 'start_date',
                                  null = True,
                                  blank = True,
                                  verbose_name = _(u'Start date'))
    end_date = models.DateField(db_column = 'end_date',
                                  null = True,
                                  blank = True,
                                  verbose_name = _(u'End date'))
    level = models.ForeignKey('CourseLevel',
                              db_column = 'id_level',
                              verbose_name = _(u'Education level'))
    type = models.ForeignKey('CourseType',
                             db_column = 'id_type',
                             verbose_name = _(u'Typ'))
    profile = models.ForeignKey('CourseProfile',
                                db_column = 'id_profile',
                                null = True,
                                blank = True,
                                verbose_name = _(u'Education profile'))
    department = models.ForeignKey('trainman.Department',
                                   db_column = 'id_department',
                                   null = True,
                                   blank = True,
                                   verbose_name = _(u'Unit'))
    is_first = models.NullBooleanField(db_column = 'is_first',
                                       default = None,
                                       verbose_name = _(u'First'))
    is_last = models.NullBooleanField(db_column = 'is_last',
                                      default = None,
                                      verbose_name = _(u'Last'))

    objects = CourseManager()

    def __unicode__(self):
        sem_desc = ""
        if self.years:
            sem_desc = "[%s %s]" % (unicode(self.years), _(u'years'))
        else:
            sem_desc = "[%s %s]" % (unicode(self.semesters), _(u'sems'))
        s = u'%s, %s %s, %s' % (unicode(self.name), unicode(self.level), sem_desc, unicode(self.type))

        if self.type.id not in (LEVEL_PG_Q, LEVEL_PG_FL) and self.profile is not None:
            s += u', %s' % unicode(self.profile)

        if self.start_date is None:
            return s
        return s + u', rozpoczęty w: %s' % self.start_date.year

    def __lt__(self, other):
        return self.start_date < other.start_date

    def __gt__(self, other):
        return self.start_date > other.start_date

    def save(self, *args, **kwargs):
        self.is_first = self.get_prev() is None
        self.is_last = self.get_next() is None
        
        if self.semesters:
            self.end_date = self.date(self.start_date, self.semesters-1, begin=False)
        elif self.years:
            self.end_date = self.date(self.start_date, self.years*2-1, begin=False)
        
        super(Course, self).save(*args, **kwargs)

    def get_start_date(self):
        return self.date(self.start_date, begin=True)

    def get_end_date(self):
        return self.date(self.end_date, begin=False)

    def get_prev(self):
        if not self.start_date:
            return None
        courses = self.get_courses().exclude(id__exact = self.id).filter(start_date__lt = self.start_date).order_by('-start_date')
        return courses[0] if len(courses) > 0 else None

    def get_next(self):
        if not self.start_date:
            return None
        courses = self.get_courses().exclude(id__exact = self.id).filter(start_date__gt = self.start_date).order_by('start_date')
        return courses[0] if len(courses) > 0 else None

    def is_course_first(self):
        return self.is_first

    def is_course_last(self):
        return self.is_last

    def get_courses(self):
        lang = getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE)
        f = {'name_%s' % lang: getattr(self, 'name_%s' % lang, '')}
        return Course.objects.active().filter(**f).filter(semesters__exact = self.semesters,
                                    years__exact = self.years,
                                    level__exact = self.level,
                                    type__exact = self.type,
                                    profile__exact = self.profile,
                                    department__exact = self.department)
        
    def get_current_semesters(self):
        semester = self.get_current_semester()
        if not semester:
            return []
        if semester % 2 == 0:
            return [semester - 1, semester]
        else:
            return [semester, semester + 1]
    
    def get_current_semester(self):
        """
        Returns current semester of this course.
        """
        if not self.didactic_offer or not self.didactic_offer.is_active:
            return None
        diff = self.diff(self.didactic_offer.start_date, self.start_date)
        if diff is None:
            return None
        return diff+1
    
    def get_current_year(self):
        semester = self.get_current_semester()
        if not semester:
            return None
        return int(math.ceil(semester / 2.0))
    
    def is_in_active_offer(self):
        """
        Returns True if course is in active didactic offer.
        """
        return self.didactic_offer and self.didactic_offer.is_active
    
    # Metody sprawdzające poziom nauczania
    def is_level_ba(self):
        return True if self.level.id == LEVEL_BA else False
    
    def is_level_msc(self):
        return True if self.level.id == LEVEL_MSC else False
    
    def is_level_phd(self):
        return True if self.level.id == LEVEL_PHD else False
    
    def is_level_u_msc(self):
        return True if self.level.id == LEVEL_U_MSC else False
    
    def is_level_eng(self):
        return True if self.level.id == LEVEL_ENG else False
    
    def is_level_pg_q(self):
        return True if self.level.id == LEVEL_PG_Q else False
    
    def is_level_pg_fl(self):
        return True if self.level.id == LEVEL_PG_FL else False

    def get_name_with_current_year(self):
        return u"%s, %s, %s, %s, rocznik: %s (rok studiów: %s)" % (self.name, self.level, self.type, self.profile, self.start_date.year, self.get_current_year())

#########################################
#          SPECIALTIES
#########################################

class SGroupType(AbstractUniqueName):
    class Meta:
        db_table = 'merv_sgroup_type'
        verbose_name = _(u'Type of subjects group')
        verbose_name_plural = _(u'Types of subjects group')

@active_manager
@didactic_offer_manager
@latest_manager
class SGroupManager(models.Manager):
    
    def active(self):
        raise NotImplementedError('decorated')
    
    def didactic_offer(self):
        raise NotImplementedError('decorated')
    
    def latest(self):
        raise NotImplementedError('decorated')

class SGroup(AbstractDidacticOffer):
    class Meta:
        db_table = 'merv_sgroup'
        ordering = ('-is_active', '-type', 'name',)
        verbose_name = _(u'Subjects group')
        verbose_name_plural = _(u'Subjects groups')

    start_semester = models.IntegerField(db_column = 'start_semester',
                                         max_length = 2,
                                         default = 1,
                                         verbose_name = _(u'First semester/year'))
    sgroup = models.ForeignKey('self',
                               db_column = 'id_sgroup',
                               related_name = '+',
                               null = True,
                               blank = True,
                               verbose_name = _(u'Parent subjects group'))
    course = models.ForeignKey('Course',
                              db_column = 'id_course',
                              verbose_name = _(u'Course'))
    type = models.ForeignKey('SGroupType',
                             db_column = 'id_type',
                             verbose_name = _(u'Type'))
    modules = models.ManyToManyField('Module',
                                     db_table = 'merv_module__to__sgroup',
                                     null = True,
                                     blank = True,
                                     verbose_name = _(u'Modules'))
    objects = SGroupManager()

    def __unicode__(self):
        from apps.merovingian.functions import default_sgroup_name
        if self.name == default_sgroup_name():
            return unicode(self.course)
        else:
            return '%s - %s' % (unicode(self.course), unicode(self.name))

    def save(self, *args, **kwargs):
        from apps.merovingian.functions import default_sgroup_name
        try:
            dsg_name = default_sgroup_name()
            if self.type.name != dsg_name:
                self.sgroup = SGroup.objects.get(course = self.course, type = SGroupType.objects.get(name = dsg_name))
        except:
            pass
        finally:
            super(SGroup, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        for m in self.modules.all():
            m.delete()
        super(SGroup, self).delete(*args, **kwargs)

    def get_start_date(self):
        if self.course.semesters:
            return self.date(self.course.start_date, self.start_semester - 1, begin=True)
        elif self.course.years:
            return self.date(self.course.start_date, (self.start_semester - 1)*2, begin=True)
        else:
            return None

    def get_end_date(self):
        return self.course.get_end_date()

    def get_prev(self):
        try:
            return self.course.get_prev().sgroup_set.get(name__iexact = self.name,
                                                        start_semester__exact = self.start_semester,
                                                        type__exact = self.type)
        except:
            return None

    def get_next(self):
        try:
            return self.course.get_next().sgroup_set.get(name__iexact = self.name,
                                                        start_semester__exact = self.start_semester,
                                                        type__exact = self.type)
        except:
            return None

    def is_course_first(self):
        return self.course.is_course_first()

    def is_course_last(self):
        return self.course.is_course_last()


#########################################
#            MODULES
#########################################

class ModuleType(AbstractUniqueName):
    class Meta:
        db_table = 'merv_module_type'
        ordering = ('name',)
        verbose_name = _(u'Module type')
        verbose_name_plural = _(u'Module types')

@active_manager
@didactic_offer_manager
@didactic_offer_and_future_manager
@latest_manager
class ModuleManager(models.Manager):
    
    def active(self):
        raise NotImplementedError('decorated')
    
    def didactic_offer(self):
        raise NotImplementedError('decorated')
    
    def latest(self):
        raise NotImplementedError('decorated')
    
    def didactic_offer_and_future(self):
        raise NotImplementedError('decorated')
    
class Module(AbstractDidacticOffer):
    class Meta:
        db_table = 'merv_module'
        ordering = ('-is_active', 'type', 'name',)
        verbose_name = _(u'Module')
        verbose_name_plural = _(u'Modules')
        ordering = ['name']

    ects = models.FloatField(db_column = 'ects',
                             null = True,
                             blank = True,
                             verbose_name = _(u'ECTS points'))
    type = models.ForeignKey('ModuleType',
                             db_column = 'id_type',
                             null = True,
                             blank = True,
                             verbose_name = _(u'Type'))
    code = models.CharField(max_length=128, blank=True)

    objects = ModuleManager()

    def get_start_date(self):
        if not self.pk:
            return None
        course = self.get_sgroup().course
        if course.semesters:
            semesters = self.get_semesters()
            semesters.append(course.semesters)
            return self.date(course.start_date, min(semesters) - 1, begin=True) if self.id is not None else None
        elif course.years: #years
            years = self.get_semesters()
            years.append(course.years)
            return self.date(course.start_date, (min(years) - 1)*2, begin=True) if self.id is not None else None
        else:
            return None

    def get_end_date(self):
        if not self.pk:
            return None
        course = self.get_sgroup().course
        if course.semesters:
            semesters = self.get_semesters()
            if len(semesters) > 0:
                semester = max(semesters)
            else:
                semester = 0
            return self.date(course.start_date, min([semester, course.semesters])-1, begin=False) if self.id is not None else None
        elif course.years: #years
            years = self.get_semesters()
            if len(years) > 0:
                year = max(years)
            else:
                year = 0
            return self.date(course.start_date, min([year, course.years])*2-1, begin=False) if self.id is not None else None
        else:
            return None

    def get_prev(self):
        try:
            return self.get_sgroup().get_prev().modules.get(name__iexact = self.name,
                                                            ects__exact = self.ects,
                                                            type__exact = self.type)
        except:
            return None

    def get_next(self):
        try:
            return self.get_sgroup().get_next().modules.get(name__iexact = self.name,
                                                            ects__exact = self.ects,
                                                            type__exact = self.type)
        except:
            return None
    
    def is_annual(self):
        return True if self.get_course().years else False

    def is_course_first(self):
        try:
            return self.get_sgroup().is_course_first()
        except:
            return False

    def is_course_last(self):
        try:
            return self.get_sgroup().is_course_last()
        except:
            return False

    def get_sgroups(self):
        return self.sgroup_set.all()

    def get_courses(self):
        return Course.objects.filter(id__in = [sg.course.id for sg in self.sgroup_set.all()]).distinct()

    def get_departments(self):
        from apps.trainman.models import Department
        return Department.objects.filter(id__in = [m.department.id for m in self.get_courses() if m.department is not None])

    def get_sgroup(self):
        sgroups = self.get_sgroups()
        return sgroups[0] if len(sgroups) > 0 else None

    def get_course(self):
        courses = self.get_courses()
        return courses[0] if len(courses) > 0 else None

    def get_department(self):
        departments = self.get_departments()
        return departments[0] if len(departments) > 0 else None

    def get_teachers(self):
        from apps.trainman.models import Teacher
        return Teacher.objects.filter(subject__in = Subject.objects.filter(module__exact = self)).distinct()

    def get_lecture_teachers(self):
        from apps.trainman.models import Teacher
        from apps.merovingian.functions import lecture_name
        return Teacher.objects.filter(subject__in = Subject.objects.filter(module__exact = self, type__name__exact = lecture_name())).distinct()

    def get_semesters(self):
        """
        Returns semesters/years list of its subjects
        """
        return [s.semester for s in Subject.objects.filter(module__exact = self)]

    def subjects_have_defined_ects(self):
        from django.db.models import Q
        s_all = Subject.objects.filter(module__exact = self).count()
        s_ects = Subject.objects.filter(module__exact = self).exclude(Q(ects__exact = 0) | Q(ects__exact = None)).count()
        return True if s_all == s_ects else False

class ModuleProperties(models.Model):
    class Meta:
        db_table = 'merv_module_properties'
        unique_together = ('semester', 'module', 'type')
        ordering = ('semester',)
        verbose_name = _(u'Module attributes')
        verbose_name_plural = _(u'Module attributes')

    semester = models.IntegerField(db_column = 'semester',
                                   max_length = 2,
                                   validators = [MinValueValidator(semester_minimal), MaxValueValidator(semester_maximal)],
                                   verbose_name = _(u'Semester/Year'))
    hours = models.FloatField(db_column = 'hours',
                              null = True,
                              blank = True,
                              verbose_name = _(u'Number of hours'))
    ects = models.FloatField(db_column = 'ects',
                             verbose_name = _(u'ECTS points'))
    type = models.ForeignKey('SubjectType',
                             db_column = 'id_type',
                             null = True,
                             blank = True,
                             verbose_name = _(u'Type of classes'))
    module = models.ForeignKey('Module',
                               db_column = 'id_module',
                               verbose_name = _(u'Module'))

    def __unicode__(self):
        return '%s, %s' % (unicode(self.module), self.semester)


#########################################
#            SUBJECTS
#########################################

class SubjectType(AbstractUniqueName):
    class Meta:
        db_table = 'merv_subject_type'
        ordering = ('name',)
        verbose_name = _(u'Type of classes')
        verbose_name_plural = _(u'Types of classes')

    def short_name(self):
        return {
            9: u'WY',
            10: u'LB',
            11: u'KW',
            12: u'ĆW',
            13: u'SM',
            14: u'PR',
            15: u'CT',
            16: u'LT',
            17: u'KS'
        }[self.id]

class SubjectAssessment(AbstractUniqueName):
    class Meta:
        db_table = 'merv_subject_assessment'
        ordering = ('name',)
        verbose_name = _(u'Type of pass')
        verbose_name_plural = _(u'Types of pass')

@active_manager
@didactic_offer_manager
@latest_manager
@didactic_offer_and_future_manager
class SubjectManager(models.Manager):
    
    def active(self):
        raise NotImplementedError('decorated')
    
    def didactic_offer(self):
        raise NotImplementedError('decorated')
    
    def latest(self):
        raise NotImplementedError('decorated')
    
    def didactic_offer_and_future(self):
        raise NotImplementedError('decorated')

class Subject(AbstractDidacticOffer):
    class Meta:
        db_table = 'merv_subject'
        ordering = ('semester', 'name', '-type', 'assessment')
        verbose_name = _(u'Subject')
        verbose_name_plural = _(u'Subjects')

    hours = models.FloatField(db_column = 'hours',
                              verbose_name = _(u'Number of hours'))
    semester = models.IntegerField(db_column = 'semester',
                                   max_length = 2,
                                   validators = [MinValueValidator(1), MaxValueValidator(10)],
                                   verbose_name = _(u'Semester/Year'))
    ects = models.FloatField(db_column = 'ects',
                             null = True,
                             blank = True,
                             verbose_name = _(u'ECTS points'))
    type = models.ForeignKey('SubjectType',
                             db_column = 'id_type',
                             verbose_name = _(u'Type of classes'))
    assessment = models.ForeignKey('SubjectAssessment',
                                   db_column = 'id_assessment',
                                   verbose_name = _(u'Type of pass'))
    module = models.ForeignKey('Module',
                               db_column = 'id_module',
                               verbose_name = _(u'Module'))
    teachers = models.ManyToManyField('trainman.Teacher',
                                      through = 'SubjectToTeacher',
                                      null = True,
                                      blank = True,
                                      verbose_name = _(u'Teachers'))
    code = models.CharField(max_length=128, blank=True)

    objects = SubjectManager()

    def __unicode__(self):
        return '%s, %sh, %s' % (self.name, self.hours, self.type)
    
    def is_annual(self):
        return True if self.module.get_course().years else False

    def get_start_date(self):
        if not self.module or not self.module.get_sgroup():
            return None
        course = self.module.get_sgroup().course
        if course.semesters:
            return self.date(course.start_date, min(self.semester, course.semesters) - 1, begin=True)
        elif course.years: # years
            return self.date(course.start_date, (min(self.semester, course.years)-1)*2, begin=True)
        else:
            return None

    def get_end_date(self):
        if not self.module or not self.module.get_sgroup():
            return None
        course = self.module.get_sgroup().course
        if course.semesters:
            return self.date(course.start_date, min(self.semester, course.semesters) - 1, begin=False)
        elif course.years: # years
            return self.date(course.start_date, min(self.semester, course.years)*2-1, begin=False)
        else:
            return None

    def get_prev(self):
        try:
            return self.module.get_prev().subject_set.get(name__iexact = self.name,
                                                          hours__exact = self.hours,
                                                          semester__exact = self.semester,
                                                          ects__exact = self.ects,
                                                          type__exact = self.type,
                                                          assessment__exact = self.assessment)
        except:
            return None                              

    def get_next(self):
        try:
            return self.module.get_next().subject_set.get(name__exact = self.name,
                                                          hours__exact = self.hours,
                                                          semester__exact = self.semester,
                                                          ects__exact = self.ects,
                                                          type__exact = self.type,
                                                          assessment__exact = self.assessment)
        except:
            return None

    def is_course_first(self):
        return self.module.is_course_first()

    def is_course_last(self):
        return self.module.is_course_last()

    def get_year_semester(self):
        if not self.module or not self.module.get_sgroup():
            return (None, None)
        course = self.module.get_sgroup().course
        
        if course.semesters:
            return (int(self.semester / 2.0 + 0.5), int((self.semester + 1) % 2 + 1))
        else:
            return (self.semester, None)

class SubjectToTeacher(models.Model):
    class Meta:
        db_table = 'merv_subject__to__teacher'
        verbose_name = _(u'Subject - Teacher')
        verbose_name_plural = _(u'Subjects - Teachers')
        
    groups = models.IntegerField(db_column = 'groups',
                                 max_length = 2,
                                 verbose_name = _(u'Number of groups'))
    hours = models.FloatField(db_column = 'hours',
                              max_length = 3,
                              verbose_name = _(u'Number of hours'))
    description = models.CharField(db_column = 'description',
                                   max_length = 512,
                                   null = True,
                                   blank = True,
                                   verbose_name = _(u'Description'))
    teacher = models.ForeignKey('trainman.Teacher',
                                db_column = 'id_teacher',
                                verbose_name = _(u'Teacher'))
    subject = models.ForeignKey('Subject',
                                db_column = 'id_subject',
                                verbose_name = _(u'Subject'))

    def __unicode__(self):
        return '%s - %s' % (unicode(self.subject), unicode(self.teacher))

#########################################
#            DEGREE PROFILE
#########################################

class CourseDegreeProfile(models.Model):
    
    class Meta:
        db_table = 'merv_degree_profile'
        verbose_name = _(u'Degree Profile')
        verbose_name_plural = _(u'Degree Profiles')
        
    course = models.OneToOneField(Course)
    
    """
    Please identify the type of degree, e.g. whether the degree is the result of 
    a programme offered by a single institution or whether the degree 
    is the result of a joint programme (joint degree or double / multiple degree). 
    Please indicate between brackets the length of the degree programme in ECTS-credits, 
    and/or - if applicable - national/institutional credits and/or years of study.
    """
    type_length      = models.TextField(verbose_name=_(u'Type and length of degree'), blank=True, default='')
    
    """
    Please give the official name of the awarding institution(s), 
    and the country where it is based (If the name is not in Latin Alphabet, 
    please provide a translit- eration or transcription. 
    In addition, please provide also an official translation in English (if applicable) in italics)
    """
    institutions     = models.TextField(verbose_name=_(u'Awarding institutions'), blank=True, default='')

    """
    Please identify the accreditation organisation(s) that provides the accreditation 
    of the degree programme or the degree awarding institution, 
    and the country in which the accreditation organisation operates.
    """
    accreditation_organisations = models.TextField(verbose_name=_(u'Accreditation organisations'), blank=True, default='')
    
    """
    Please identify the year(s) for which the curriculum is validated/approved.
    RETREIVED FROM TECHING OFFER MODEL
    """
    
    """
    Please indicate the main discipline(s) / subject area(s) of the degree programme. 
    If the programme is multi- or interdisciplinary, 
    please indicate the relative weight of the major components, 
    if applicable (e.g. politics, law and economics (60:20:20).
    """
    disciplines     = models.TextField(verbose_name=_(u'Disciplines / Subject Areas'), blank=True, default='')
    
    """
    Please provide (in 2 sentences) a general statement about the degree programme,
    providing a short summary of the overall purpose of the programme.
    """
    purpose         = models.TextField(verbose_name=_(u'Purpose of the programme'), blank=True, default='')
    
    focus           = models.TextField(verbose_name=_(u'General and/or specialist focus'), blank=True, default='')
    
    """
    Please outline the orientation of the degree programme. 
    For example whether the degree is primarily research, practically based, 
    professional, applied, related to designated employment, etc.
    """
    orientation     = models.TextField(verbose_name=_(u'Orientation'), blank=True, default='')
    
    """
    Please indicate any additional features that distinguish this degree programme 
    from other similar degree programmes. 
    For example: if the programme includes a compulsory international component, 
    a work placement, a specific environment or is taught in a second language.
    """
    distinctive_features = models.TextField(verbose_name=_(u'Distinctive features'), blank=True, default='')
    
    """
    Please summarise (in maximum 3 lines) the main employment opportunities 
    that arise from successful completion of the programme. 
    Indicate whether the award confers any nationally regulated/protected 
    title on the holder and if so, provide more information about the title and rights attached. 
    Indicate if the title is protected by law.
    """
    employability       = models.TextField(verbose_name=_(u'Employability'), blank=True, default='')
    
    """
    Please indicate (in maximum 3 lines) opportunities for access to further studies, 
    both, within and outside the main and specific subject areas identified above (B.1.).
    """
    further_studies     = models.TextField(verbose_name=_(u'Further studies'), blank=True, default='')
    
    """
    Please indicate (in maximum 3 lines) the main learning and teaching strategies and methods.
    """
    teaching_approaches = models.TextField(verbose_name=_(u'Learning and teaching approaches'), blank=True, default='')
    
    """
    Please indicate (in maximum 3 lines) the main assessment strategies and methods.
    """
    assessment_methods  = models.TextField(verbose_name=_(u'Assessment methods'), blank=True, default='')
    
    
    def __unicode__(self):
        return unicode(self.course)

#########################################
#            DEGREE PROFILE
#########################################

class CourseDiagnosticInfo(models.Model):
    
    class Meta:
        db_table = 'merv_diagnostic_info'
        verbose_name = _(u'Diagnostic Information')
        verbose_name_plural = _(u'Diagnostic Informations')
        
    course = models.OneToOneField(Course)
    
    minimal_ects_per_semester = models.IntegerField(verbose_name=_(u'Minimal number of ECTS points for each semester'))
    maximal_hours = models.IntegerField(verbose_name=_(u'Maximal number of hours in all semesters'))
    maximal_exams_per_semester = models.IntegerField(verbose_name=_(u'Maximal number of exams for each semester'))

#########################################
#            SIGNALS
#########################################

def set_didactic_offer(sender, **kwargs):
    """
    Signal sets the didactic offer for the sender depending on the start and end date.
    
    Tries to find the didactic offer for instance. 
    Algorithm:
        Get all didactic offers from newest to latest
        For each of them:
            Check if the period of instance overlaps the period of this didactif offer
            If yes:
                Set this didactic offer as instance's offer
            If active offer was checked:
                Do not set and omit other didactic offers
    """
    if kwargs.get('raw', True):
        return
    
    if not 'instance' in kwargs:
        return
    instance = kwargs.get('instance')
    if not instance:
        return
    
    instance.didactic_offer = None
    object_start_date   = instance.get_start_date()
    object_end_date     = instance.get_end_date()
    
    if object_start_date is None or object_end_date is None:
        return

    doffers = DidacticOffer.objects.order_by('-start_date')
    for doffer in doffers:
        doffer_start_date, doffer_end_date = doffer.start_date, doffer.end_date
        
        if object_start_date > doffer_end_date:
            break
    
        if object_start_date <= doffer_end_date and object_end_date >= doffer_start_date:
            instance.didactic_offer = doffer
            if doffer.is_active:
                break

models.signals.pre_save.connect(set_didactic_offer, sender = Course)
models.signals.pre_save.connect(set_didactic_offer, sender = SGroup)
models.signals.pre_save.connect(set_didactic_offer, sender = Module)
models.signals.pre_save.connect(set_didactic_offer, sender = Subject)

def set_default_sgroup(sender, **kwargs):
    """
    Signal adds default specialty that exists in every course. 
    It's name is configurable.
    """
    if not kwargs.get('created', True) or kwargs.get('raw', False):
        return
    from apps.merovingian.functions import default_sgroup_settings
    dsg_settings = default_sgroup_settings()
    
    kwargs_names_exprs = {}
    kwargs_names = {}
    for lang_code, lang_name in settings.LANGUAGES:
        kwargs_names_exprs['name_'+lang_code+'__exact'] = getattr(dsg_settings, 'value_'+lang_code)
        kwargs_names['name_'+lang_code] = getattr(dsg_settings, 'value_'+lang_code)
    
    default_sgroup_type, created = SGroupType.objects.get_or_create(**kwargs_names_exprs)
    
    SGroup.objects.create(is_active = True,
                          course = kwargs.get('instance', Course.objects.none()),
                          type = default_sgroup_type, **kwargs_names)

models.signals.post_save.connect(set_default_sgroup, sender = Course)
