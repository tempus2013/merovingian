# -*- coding: utf-8 -*-

from django.conf.urls.defaults import patterns
from django.conf.urls import url

urlpatterns = patterns(
    'apps.merovingian.views',
    (r'^course/list/$', 'course.index'),
    (r'^course/list/(?P<faculty_id>\d+)$', 'course.index'),
    (r'^course/add/?$', 'course.add'),
    (r'^course/name/(?P<course_id>\d+)/$', 'course.name'),
    url(r'^course/(?P<course_id>\d+)/$', 'course.show', name='show'),
    (r'^course/(?P<course_id>\d+)/edit/$', 'course.edit'),
    (r'^course/(?P<course_id>\d+)/get_data/$', 'course.get_data'),

    (r'^course/specialty/(?P<sgroup_id>\d+)/module/list/$', 'module.list'),
    (r'^course/specialty/(?P<sgroup_id>\d+)/module/add/$', 'module.add'),
    (r'^course/specialty/(?P<sgroup_id>\d+)/module/(?P<module_id>\d+)/edit/$', 'module.edit'),
    (r'^course/specialty/(?P<sgroup_id>\d+)/module/(?P<module_id>\d+)/delete/$', 'module.delete'),
    
    (r'^course/studies_plan/(?P<sgroup_id>\d+)/$', 'sgroup.studies_plan'),
    (r'^course/studies_plan/(?P<sgroup_id>\d+)/syllabus/(?P<module_id>\d+)$', 'module.syllabuses'),
    
    (r'^course/profile/(?P<course_id>\d+)/$', 'profile.show'),
    (r'^course/profile/(?P<course_id>\d+)/edit/$', 'profile.edit'),
    
    (r'^course/diagnostic/(?P<course_id>\d+)/rules/$', 'diagnostic.edit'),
    
    (r'management/list/', 'management.index'),
    
    (r'^delete/confirm/$', 'delete_confirm'),
    (r'management/', 'management'),
    (r'', 'index'),
)
