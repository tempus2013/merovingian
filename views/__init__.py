# -*- coding: utf-8 -*-

from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _

from apps.merovingian.models import MerovingianNews
from django.core import urlresolvers

# --- Main Views ---

def index(request):
    return redirect(urlresolvers.reverse('apps.merovingian.views.course.index'))

@login_required
def management(request):
    return redirect(urlresolvers.reverse('apps.merovingian.views.management.index'))

@login_required
def news(request):
    news = MerovingianNews.objects.filter(is_active = True)
    kwargs = {'news': news}
    return render_to_response('merovingian/news/list.html',
                              kwargs,
                              context_instance = RequestContext(request))

@login_required
def delete_confirm(request):
    if request.method == 'POST':
        if request.POST.get('action', '') == 'yes':
            return redirect(request.session.pop('confirm_yes', ''))
        elif request.POST.get('action', '') == 'no':
            return redirect(request.session.pop('confirm_no', ''))
    else:
        request.session['confirm_yes'] = request.GET.get('yes', '')
        request.session['confirm_no'] = request.GET.get('no', '')
        kwargs = {'text': _(u'Selected item will be removed. Are you sure?')}
        return render_to_response('merovingian/confirm.html',
                                  kwargs,
                                  context_instance = RequestContext(request))
