# -*- coding: utf-8 -*-

from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.translation import ugettext as _

from apps.merovingian.models import *
from apps.merovingian.forms import *
from apps.merovingian.functions import *
from django.db import transaction


@login_required
@permission_required('merovingian.change_coursediagnosticinfo')
def edit(request, course_id):
    """
    """
    try:
        course = Course.objects.active().get(id = course_id)
    except Course.DoesNotExist:
        messages.error(request, _(u'Internal error, please contact the Administrator.') + ' #ps0.')
        return redirect('apps.merovingian.views.course.list')
    
    try:
        diagnostic_info = course.coursediagnosticinfo
    except CourseDiagnosticInfo.DoesNotExist:
        diagnostic_info = CourseDiagnosticInfo()
        diagnostic_info.course = course
        
    form = DiagnosticInfoForm(instance=diagnostic_info)
    if request.method == "POST":
        form = DiagnosticInfoForm(instance=diagnostic_info, data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, _(u'Diagnostic rules has been saved succesfully.'))
            return redirect('apps.merovingian.views.diagnostic.edit', course_id = course.id)
        else:
            print form.errors
            messages.error(request, _(u'Correct errors listed below.'))
    
    kwargs = {'course': course, 'form': form}
    return render_to_response('merovingian/diagnostic/edit.html',
                              kwargs,
                              context_instance = RequestContext(request))
    