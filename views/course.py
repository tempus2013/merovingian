# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from django.template import RequestContext
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.utils import translation
from django.utils.translation import ugettext as _
from django.db import transaction
from django.http import HttpResponse

from apps.trainman.models import UserProfile, Department
from apps.merovingian.functions import user_courses, make_page,\
    default_sgroup_name, elective_facultative_module_name, examination_name
from apps.merovingian.forms import SearchForm, SearchFilterForm, CourseForm, SGroupInlineFormset
from apps.merovingian.models import SGroup, MerovingianAdmin, Course,\
    CourseDiagnosticInfo, ModuleProperties
from django.db.models.aggregates import Max

from apps.trinity.views.trinity import is_learning_outcomes_administrator
from django.http import HttpResponseNotFound

# --- Courses Views ---


def index(request, faculty_id=None):
    
    column_name = 'name_' + translation.get_language()
    courses_names = Course.objects.didactic_offer_and_future().values(column_name).distinct().annotate(id=Max('id')).order_by(column_name)

    course_name = request.session.get('merv_courses_names_search', '')

    if request.method == 'POST':
        search_form = SearchFilterForm(request.POST)
        if search_form.is_valid():
            course_name = request.session['merv_courses_names_search'] = search_form.cleaned_data['name']
            faculty = search_form.cleaned_data['faculty']
            if faculty is None:
                return redirect(reverse('apps.merovingian.views.course.index'))
            else:
                return redirect(reverse('apps.merovingian.views.course.index', kwargs={'faculty_id': faculty.id}))
    else:
        search_form = SearchFilterForm(initial={'name': course_name, 'faculty': faculty_id})

    if course_name:
        filters = {column_name+'__icontains': course_name}
        courses_names = courses_names.filter(**filters)

    if faculty_id is not None:
        faculty = get_object_or_404(Department, id=faculty_id)
        departments_ids = faculty.children_id()
        courses_names = courses_names.filter(department__id__in=departments_ids)

    courses_names_page = make_page(request, courses_names, 'merv_courses_names')
    courses_names = [{'id': m['id'], 'name': m[column_name]} for m in courses_names_page.object_list]

    kwargs = {'courses_names_page': courses_names_page, 'courses_names': courses_names, 'search_form': search_form}
    return render_to_response('merovingian/courses/index.html', kwargs, context_instance=RequestContext(request))

@login_required
@permission_required('merovingian.add_course')
def add(request):
    course = None
    course_form = CourseForm(request.POST or None)
    sgroup_formset = SGroupInlineFormset(request.POST or None, request.FILES or None)
    if request.method == 'POST' and course_form.is_valid() and sgroup_formset.is_valid():
        with transaction.commit_on_success():
            course = course_form.save()
            # Create formset again to assign course instance
            sgroup_formset = SGroupInlineFormset(request.POST or None, request.FILES or None, instance = course)
            sgroup_formset.save()
            if not request.user.is_superuser:
                try:
                    (merovingian_admin, created) = MerovingianAdmin.objects.get_or_create(user_profile = request.user.get_profile())
                except UserProfile.DoesNotExist:
                    user_profile = UserProfile(user = request.user)
                    user_profile.save()
                    merovingian_admin = MerovingianAdmin.objects.create(user_profile = user_profile)
                    merovingian_admin.courses.add(course)
        messages.success(request, _(u'Course %s has been saved succesfully.') % course)
    elif request.method == 'POST':
        messages.error(request, _(u'Correct errors listed below.'))

    if request.POST.get('action') == 'save' and course_form.is_valid() and sgroup_formset.is_valid():
        return redirect('apps.merovingian.views.course.show', course_id = course.id)
    elif request.POST.get('action') == 'save_and_edit' and course_form.is_valid() and sgroup_formset.is_valid():
        return redirect('apps.merovingian.views.course.edit', course_id = course.id)
    else:
        kwargs = {'course': course,
                  'course_form': course_form,
                  'sgroup_formset': sgroup_formset}
        return render_to_response('merovingian/courses/course_form.html',
                                  kwargs,
                                  context_instance = RequestContext(request))

def name(request, course_id):
    """"""
    try:
        course = Course.objects.didactic_offer_and_future().get(id = course_id)
    except Course.DoesNotExist:
        messages.error(request, _(u'Selected course does not exist. If you see this message again, contact the Administrator.') +' #m0e1')
        return redirect('apps.merovingian.views.course.index')
    except:
        messages.error(request, _(u'Internal error, please contact the Administrator.') +' #m0e0')
        return redirect('apps.merovingian.views.course.index')
        
    all_courses = Course.objects.didactic_offer_and_future()\
                        .filter(name=course.name)\
                        .order_by('-start_date', 'level')
                        
    if len(all_courses) == 0:
        newest_course = course
    else:
        newest_course = all_courses[0]
    return redirect('apps.merovingian.views.course.show', newest_course.id)
    

def show(request, course_id):
    """
    """

    show_archived = True if request.GET.get('archived', False) else False

    def count_exams_per_semester(sgroup, whole_course_sgroup=None, semesters=1):
        """ """
        modules = list(sgroup.modules.filter(is_active=True).all())
        if whole_course_sgroup:
            modules.extend(list(whole_course_sgroup.modules.filter(is_active=True).all()))
        
        semesters_exams = {}
        
        exam_assesment_name = examination_name()
        
        for m in modules:
            for s in m.subject_set.filter(is_active=True).all():
                if s.semester not in semesters_exams:
                    semesters_exams[s.semester] = 0
                if s.assessment.name == exam_assesment_name:
                    semesters_exams[s.semester] += 1 

        ret = []
        for i in range(1, semesters+1):
            if i in semesters_exams:
                ret.append(semesters_exams[i])
            else:
                ret.append(0)
        return ret
    
    def count_ects_per_semester(sgroup, whole_course_sgroup=None, semesters=1):
        """ """
        modules = list(sgroup.modules.filter(is_active=True).all())
        if whole_course_sgroup:
            modules.extend(list(whole_course_sgroup.modules.filter(is_active=True).all()))
        
        semesters_ects = {}
        
        facultative_module_type = elective_facultative_module_name()
        
        for m in modules:
            # Get subjects list 
            # or module properties if it is elective ects module
            subjects = m.subject_set.filter(is_active=True).all()
            if m.type and m.type.name == facultative_module_type:
                try:
                    subjects = ModuleProperties.objects.filter(module=m)
                except ModuleProperties.DoesNotExist:
                    subjects = []
             
            for s in subjects:
                if s.semester not in semesters_ects:
                    semesters_ects[s.semester] = 0
                if s.ects:
                    semesters_ects[s.semester] += s.ects 

        ret = []
        for i in range(1, semesters+1):
            if i in semesters_ects:
                ret.append(int(semesters_ects[i]))
            else:
                ret.append(0)
        return ret
    
    def count_hours(sgroup, whole_course_sgroup=None, semesters=1):
        """ """
        modules = list(sgroup.modules.filter(is_active=True).all())
        if whole_course_sgroup:
            modules.extend(list(whole_course_sgroup.modules.filter(is_active=True).all()))
        
        hours = 0
        
        facultative_module_type = elective_facultative_module_name()
        
        for m in modules:
            # Get subjects list 
            # or module properties if it is elective ects module
            subjects = m.subject_set.filter(is_active=True).all()
            if m.type and m.type.name == facultative_module_type:
                try:
                    subjects = ModuleProperties.objects.filter(module=m)
                except ModuleProperties.DoesNotExist:
                    subjects = []
             
            for s in subjects:
                if s.hours:
                    hours += s.hours 
        return int(hours)

    course_manager = Course.objects.all() if show_archived else Course.objects.didactic_offer_and_future()

    try:
        course = course_manager.get(id = course_id)
    except Course.DoesNotExist:
        messages.error(request, _(u'Selected course does not exist. If you see this message again, contact the Administrator.') +' #m0e1')
        return redirect('apps.merovingian.views.course.index')
    except:
        messages.error(request, _(u'Internal error, please contact the Administrator.') +' #m0e0')
        return redirect('apps.merovingian.views.course.index')
        
    # All other (year, level) courses for this course
    all_courses = course_manager.filter(name=course.name).order_by('-start_date', 'level')
                   
    # Other years and levels     
    courses_years = []
    for m in all_courses:
        if m.start_date:
            year = m.start_date.year
            if year not in [y['class_year'] for y in courses_years]:
                courses_years.append({
                    'class_year': year,
                    'study_year': m.get_current_year()
                })
            else:
                if m.get_current_year() is not None:
                    filter(lambda z: z['class_year'] == year, courses_years)[0]['study_year'] = m.get_current_year()
                
    # Merovingian admin check
    user_merv_admin = None
    if request.user.is_authenticated():
        try:
            user_merv_admin = MerovingianAdmin.objects.get(user_profile=request.user.get_profile()) 
        except MerovingianAdmin.DoesNotExist:
            user_merv_admin = None
    
    # Learning outcomes administrator check
    is_learning_outcomes_admin = is_learning_outcomes_administrator(request.user, course)
        
    # Diagnostic informations    
    try:
        diagnostic_rules = CourseDiagnosticInfo.objects.get(course=course)
    except CourseDiagnosticInfo.DoesNotExist:
        diagnostic_rules = None
        
    sgroups_diagnostic_info = []
    diagnostic_semesters = course.semesters if course.semesters else course.years 
    # If user has right to edit course
    if diagnostic_rules and (user_merv_admin or request.user.is_superuser):
        # Load diagnostic info
        sgroups = course.sgroup_set.active()
        
        # Find sgroup for whole course
        # and save specialities in other list
        whole_course_sgroup = None
        specialties_sgroups = []
        for sgroup in sgroups:
            if sgroup.name == default_sgroup_name():
                whole_course_sgroup = sgroup
            else:
                specialties_sgroups.append(sgroup)
            
        if len(specialties_sgroups) == 0:
            sgroups_diagnostic_info.append((None, {
                'hours':    count_hours(whole_course_sgroup, 
                                        semesters=diagnostic_semesters),
                'ects':     count_ects_per_semester(whole_course_sgroup, 
                                                    semesters=diagnostic_semesters),
                'exams':    count_exams_per_semester(whole_course_sgroup, 
                                                     semesters=diagnostic_semesters),
            }))
        else:
            for current_sgroup in specialties_sgroups:
                sgroups_diagnostic_info.append((current_sgroup, {
                    'hours':    count_hours(current_sgroup, whole_course_sgroup, 
                                            semesters=diagnostic_semesters),
                    'ects':     count_ects_per_semester(current_sgroup, whole_course_sgroup, 
                                            semesters=diagnostic_semesters),
                    'exams':    count_exams_per_semester(current_sgroup, whole_course_sgroup, 
                                            semesters=diagnostic_semesters),
                }))
        
    # Template variables
    kwargs = {'course': course,
              'diagnostic_rules': diagnostic_rules,
              'sgroups_diagnostic_info': sgroups_diagnostic_info,
              'diagnostic_semesters': range(1, diagnostic_semesters+1),
              'default_sgroup_name': default_sgroup_name(),
              'courses_years': courses_years,
              'all_courses': all_courses,
              'user_merv_admin': user_merv_admin,
              'is_course_in_active_offer': course.is_in_active_offer(), 
              'is_learning_outcomes_admin': is_learning_outcomes_admin,
              'show_archived': show_archived,
    }
    return render_to_response('merovingian/courses/show.html',
                              kwargs,
                              context_instance = RequestContext(request))

@login_required
@permission_required('merovingian.change_course')
def edit(request, course_id, sgroup_id=None):
    
    try:
        course = Course.objects.get(id = course_id)
        
        # Merovingian admin check
        user_merv_admin = None
        if request.user.is_authenticated():
            try:
                user_merv_admin = MerovingianAdmin.objects.get(user_profile=request.user.get_profile()) 
            except MerovingianAdmin.DoesNotExist:
                user_merv_admin = None
                
        if not request.user.is_superuser:
            if not user_merv_admin or course not in user_merv_admin.courses.all():
                return HttpResponseNotFound()
        
        # Check if course is active and forbid access
        if course.is_in_active_offer() and not request.user.is_superuser and not user_merv_admin.temporary_privileged_access: 
            messages.error(request, _(u'You cannot edit course that is in active teaching offer.'))
            return redirect('apps.merovingian.views.course.show', course_id = course.id)
        
        if request.method == 'POST':
            course_form = CourseForm(request.POST, instance = course)
            sgroup_formset = SGroupInlineFormset(request.POST, request.FILES, instance = course)
            if course_form.is_valid() and sgroup_formset.is_valid():
                with transaction.commit_on_success():
                    course_form.save()
                    sgroup_formset.save()
                messages.success(request, _(u'Course %s has been saved succesfully.') % course)
            else:
                messages.error(request, _(u'Correct errors listed below.'))
        else:
            course_form = CourseForm(instance = course)
            sgroup_formset = SGroupInlineFormset(instance = course)
    except Course.DoesNotExist:
        messages.error(request, _(u'Selected course does not exist. If you see this message again, contact the Administrator.') +' #m0e1')
        return redirect('apps.merovingian.views.course_show')
    except:
        messages.error(request, _(u'Internal error, please contact the Administrator.') +' #m0e0')
        return redirect('apps.merovingian.views.course_show')
    else:
        try:
            sgroup = None
            if sgroup_id:
                sgroup = SGroup.objects.get(id = sgroup_id, course = course)
        except:
            pass
        
        if request.POST.get('action') == 'save' and course_form.is_valid() and sgroup_formset.is_valid():
            return redirect('apps.merovingian.views.course.show', course_id = course.id)
        elif request.POST.get('action') == 'save_and_edit' and course_form.is_valid() and sgroup_formset.is_valid():
            return redirect('apps.merovingian.views.course.edit', course_id = course.id)
        else:
            
            
            kwargs = {'course': course,
                      'course_form': course_form,
                      'sgroup_formset': sgroup_formset,
                      'sgroup': sgroup}
            return render_to_response('merovingian/courses/course_form.html',
                                      kwargs,
                                      context_instance = RequestContext(request))

def get_data(request, course_id):
    import csv
    import io

    course = get_object_or_404(Course, id=course_id)

    output = io.BytesIO()
    writer = csv.writer(output, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
    for speciality in course.sgroup_set.all():
        for module in speciality.modules.all():
            for subject in module.subject_set.all():
                row = []
                row.append(course.start_date.year)
                row.append(unicode(course.name).encode('utf-8'))
                row.append(unicode(course.level.name).encode('utf-8'))
                row.append(unicode(course.type.name).encode('utf-8'))
                row.append(speciality.id)
                row.append(unicode(speciality.name).encode('utf-8'))
                row.append(module.id)
                row.append(unicode(module.name).encode('utf-8'))
                row.append(subject.id)
                row.append(unicode(subject.name).encode('utf-8'))
                row.append(unicode(subject.type.name).encode('utf-8'))
                row.append(subject.semester)
                writer.writerow(row)

    response = HttpResponse(output.getvalue(), mimetype='text/csv')
    response['Content-Disposition'] = 'filename=data.csv'
    return response