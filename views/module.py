# -*- coding: utf-8 -*-

from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Q
from django.utils.translation import ugettext as _
from django.db import transaction

from apps.merovingian.models import *
from apps.merovingian.forms import *
from apps.merovingian.functions import *
from apps.metacortex.models import SyllabusModule, SyllabusSubject


def syllabuses(request, sgroup_id, module_id):
    try:
        sgroup = SGroup.objects.get(id__exact = sgroup_id)
        module = Module.objects.get(id__exact = module_id)
        syllabus_module = SyllabusModule.objects_published.get(module__exact = module)
    except Module.DoesNotExist, SGroup.DoesNotExist:
        messages.error(request, _(u'Internal error, please contact the Administrator.') + ' #sm0p0.')
        return redirect('apps.merovingian.views.study_sgroup_show')
    except SyllabusModule.DoesNotExist:
        syllabus_module = None
    kwargs = {'course': sgroup.course,
              'sgroup': sgroup,
              'default_sgroup_name': default_sgroup_name(),
              'module': module,
              'syllabus_module': syllabus_module,
              'syllabus_subject': [{'s': s, 'ss': SyllabusSubject.objects_published.filter(subject__exact = s)} for s in module.subject_set.all()],
              'plan': {'sgroup': sgroup}}
    return render_to_response('merovingian/modules/syllabuses.html',
                              kwargs,
                              context_instance = RequestContext(request))

@login_required
def list(request, sgroup_id):
    try:
        sgroup = SGroup.objects.get(id = sgroup_id)
        
        # Merovingian admin check
        user_merv_admin = None
        if request.user.is_authenticated():
            try:
                user_merv_admin = MerovingianAdmin.objects.get(user_profile=request.user.get_profile()) 
            except MerovingianAdmin.DoesNotExist:
                user_merv_admin = None
        
        # Check if course is active and forbid access
        if sgroup.course.is_in_active_offer() and not (request.user.is_superuser or user_merv_admin.temporary_privileged_access):
            messages.info(request, _(u'You may only modify the facultative modules, because this course is already running.'))
        
        # Handle search form
        if request.method == 'POST':
            search_form = SearchForm(request.POST)
            if search_form.is_valid():
                name = request.session['sgroup_module_' + str(sgroup_id)] = search_form.cleaned_data['name']
        else:
            name = request.session.get('sgroup_module_' + str(sgroup_id), '')
            search_form = SearchForm(initial = {'name': name})

        modules = sgroup.modules
        # Choose only facultative modules as the course is already running
        if sgroup.course.is_in_active_offer() and not (request.user.is_superuser or user_merv_admin.temporary_privileged_access):
            modules = modules.filter(type__name__exact=elective_facultative_module_name())
        
        modules = modules.filter(Q(name__istartswith = name) | Q(type__name__istartswith = name))
    except SGroup.DoesNotExist:
        messages.error(request, _(u'Selected specialty does not exist. If you see this message again, contact the Administrator.') + ' #sg0m1')
        return redirect('apps.merovingian.views.course_show')
    except:
        messages.error(request, _(u'Internal error, please contact the Administrator.') + ' #sg0m0')
        return redirect('apps.merovingian.views.course_show')
    else:
        kwargs = {'sgroup': sgroup, 
                  'course': sgroup.course,
                  'default_sgroup_name': default_sgroup_name(),
                  'modules_page': make_page(request, modules, 'sgroup_' + str(sgroup_id)),
                  'search_form': search_form,
                  'user_merv_admin': user_merv_admin}
        return render_to_response('merovingian/modules/list.html',
                                  kwargs,
                                  context_instance = RequestContext(request))
        
@login_required
@permission_required('merovingian.add_module')
def add(request, sgroup_id):
    try:
        sgroup = SGroup.objects.get(id = sgroup_id)
        
        # Merovingian admin check
        user_merv_admin = None
        if request.user.is_authenticated():
            try:
                user_merv_admin = MerovingianAdmin.objects.get(user_profile=request.user.get_profile()) 
            except MerovingianAdmin.DoesNotExist:
                user_merv_admin = None
        
        # Check if course is active and forbid access
        if sgroup.course.is_in_active_offer() and not (request.user.is_superuser or user_merv_admin.temporary_privileged_access):
            messages.error(request, _(u'You cannot edit course that is in active teaching offer.'))
            return redirect('apps.merovingian.views.course.show', course_id = sgroup.course.id)
        
        module = None
        module_form = ModuleForm()
        subject_formset = SubjectInlineFormset()
        
        if request.method == 'POST':
            # Bind course and subjects forms wit post data
            module_form = ModuleForm(request.POST)
            subject_formset = SubjectInlineFormset(request.POST, request.FILES, instance = module)
            if module_form.is_valid() and subject_formset.is_valid():
                with transaction.commit_on_success():
                    #Save module
                    module = module_form.save()
                    sgroup.modules.add(module)
                    #Formset binded again to bind saved module
                    subject_formset = SubjectInlineFormset(request.POST, request.FILES, instance = module)
                    subject_formset.save()
                    # Save module again to update module's didactic offer
                    module.save()
                
                messages.success(request, _(u'Module %s has been added succesfully.') % module)
            else:
                messages.error(request, _(u'Correct errors listed below.'))
    except SGroup.DoesNotExist:
        messages.error(request, _(u'Selected specialty does not exist. If you see this message again, contact the Administrator.') + ' #sg0ma1')
        return redirect('apps.merovingian.views.course.index')
    except:
        messages.error(request, _(u'Internal error, please contact the Administrator.') + ' #sg0ma0')
        return redirect('apps.merovingian.views.module.list', sgroup_id = sgroup.id)
    else:
        if request.POST.get('action') == 'save' and module_form.is_valid() and subject_formset.is_valid():
            return redirect('apps.merovingian.views.module.list', sgroup_id = sgroup.id)
        elif request.POST.get('action') == 'save_and_edit' and module_form.is_valid() and subject_formset.is_valid():
            return redirect('apps.merovingian.views.module.edit', sgroup_id = sgroup.id, module_id = module.id)
        else:
            kwargs = {'sgroup': sgroup,
                      'course': sgroup.course,
                      'default_sgroup_name': default_sgroup_name(),
                      'module': module,
                      'module_form': module_form,
                      'subject_formset': subject_formset}
            return render_to_response('merovingian/modules/form.html',
                                      kwargs,
                                      context_instance = RequestContext(request))
@login_required
@permission_required('merovingian.change_module')
def edit(request, sgroup_id, module_id):
    
    def is_module_properties(module):
        """
        Returns True if module is elective
        """
        if module.type is None:
            return False
        else:
            return True if module.type.name == elective_facultative_module_name() else False
    
    try:
        sgroup = SGroup.objects.get(id = sgroup_id) # modules__id__contains = module_id)
        
        # Merovingian admin check
        user_merv_admin = None
        if request.user.is_authenticated():
            try:
                user_merv_admin = MerovingianAdmin.objects.get(user_profile=request.user.get_profile()) 
            except MerovingianAdmin.DoesNotExist:
                user_merv_admin = None
        
        module = Module.objects.get(id = module_id)
        
        # Check if course is active and forbid access
        if sgroup.course.is_in_active_offer() and (module.type and module.type.name != elective_facultative_module_name()) and not (request.user.is_superuser or user_merv_admin.temporary_privileged_access):
            messages.error(request, _(u'You cannot modify modules that belongs to course which is in active teaching offer.'))
            return redirect('apps.merovingian.views.course.show', course_id = sgroup.course.id)
        
        if request.method == 'POST':
            module_form = ModuleForm(request.POST, instance = module)
            subject_formset = SubjectInlineFormset(request.POST, request.FILES, instance = module)
            module_properties_formset = ModulePropertiesInlineFormset(request.POST, request.FILES, instance = module)
            if module_form.is_valid() and subject_formset.is_valid() and module_properties_formset.is_valid():
                with transaction.commit_on_success():
                    # Save module
                    module_form.save()
                    # Rebind saved module to subjects formset 
                    subject_formset = SubjectInlineFormset(request.POST, request.FILES, instance = module)
                    subject_formset.save()
    
                    # Save module properties if module's is elective                  
                    if is_module_properties(module):
                        # Rebind saved module to properties formset
                        module_properties_formset = ModulePropertiesInlineFormset(request.POST, request.FILES, instance = module)
                        module_properties_formset.save()
                    else:
                        # Remove properties if module is no longer elective
                        ModuleProperties.objects.filter(module = module).delete()
                messages.success(request, _(u'Module %s has been saved succesfully.') % module)
            else:
                messages.error(request, _(u'Correct errors listed below.'))
        else:
            module_form = ModuleForm(instance = module)
            subject_formset = SubjectInlineFormset(instance = module)
            module_properties_formset = ModulePropertiesInlineFormset(instance = module)
    except SGroup.DoesNotExist:
        messages.error(request, _(u'Selected specialty does not exist. If you see this message again, contact the Administrator.') + ' #sg0me2')
        return redirect('apps.merovingian.views.course.index')
    except Module.DoesNotExist:
        messages.error(request, _(u'Selected module does not exist. If you see this message again, contact the Administrator.') + ' #sg0me1')
        return redirect('apps.merovingian.views.module.list', sgroup_id = sgroup.id)
    except:
        messages.error(request, _(u'Internal error, please contact the Administrator.') + ' #sg0me0')
        return redirect('apps.merovingian.views.course.index')
    else:
        if request.POST.get('action') == 'save' and module_form.is_valid() and subject_formset.is_valid() and module_properties_formset.is_valid():
            return redirect('apps.merovingian.views.module.list', sgroup_id = sgroup.id)
        elif request.POST.get('action') == 'save_and_edit' and module_form.is_valid() and subject_formset.is_valid() and module_properties_formset.is_valid():
            return redirect('apps.merovingian.views.module.edit', sgroup_id = sgroup.id, module_id = module.id)
        else:
            kwargs = {'sgroup': sgroup,
                      'course': sgroup.course,
                      'default_sgroup_name': default_sgroup_name(),
                      'module': module,
                      'module_form': module_form,
                      'subject_formset': subject_formset,
                      'module_properties': is_module_properties(module),
                      'module_properties_formset': module_properties_formset}
            return render_to_response('merovingian/modules/form.html',
                                      kwargs,
                                      context_instance = RequestContext(request))
        
@login_required
@permission_required('merovingian.delete_module')
def delete(request, sgroup_id, module_id):
    try:
        sgroup = SGroup.objects.get(id__exact = sgroup_id)
        module = Module.objects.get(id__exact = module_id)
        
        # Merovingian admin check
        user_merv_admin = None
        if request.user.is_authenticated():
            try:
                user_merv_admin = MerovingianAdmin.objects.get(user_profile=request.user.get_profile()) 
            except MerovingianAdmin.DoesNotExist:
                user_merv_admin = None
        
        if not request.user.is_superuser:
            user_courses(request.user).get(id__exact = sgroup.course_id)
            
        # Check if course is active and forbid access
        if sgroup.course.is_in_active_offer() and not (request.user.is_superuser or user_merv_admin.temporary_privileged_access):
            messages.error(request, _(u'You cannot edit course that is in active teaching offer.'))
            return redirect('apps.merovingian.views.course.show', course_id = sgroup.course.id)

    except SGroup.DoesNotExist:
        messages.error(request, _(u'Selected specialty does not exist. If you see this message again, contact the Administrator.') + ' #sg0md2')
        return redirect('apps.merovingian.views.course.index')
    except Module.DoesNotExist:
        messages.error(request, _(u'Selected module does not exist. If you see this message again, contact the Administrator.') + ' #sg0md1')
        return redirect('apps.merovingian.views.module.list', sgroup_id = sgroup.id)
    except:
        messages.error(request, _(u'Internal error, please contact the Administrator.') + ' #sg0md0')
        return redirect('apps.merovingian.views.course.show', course_id = sgroup.course.id)
    else:
        messages.success(request, _(u'Module %s has been deleted successfully.') % module)
        module.delete()
        return redirect('apps.merovingian.views.module.list', sgroup_id = sgroup.id)
