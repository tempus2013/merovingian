# -*- coding: utf-8 -*-

from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.translation import ugettext as _

from apps.merovingian.models import *
from apps.merovingian.forms import *
from apps.merovingian.functions import *
from apps.trinity.models import EducationCategory, CourseLearningOutcome
from django.db import transaction

def show(request, course_id):
    """
    Preview course profile
    """
    
    def _course_effects(course):
        edu_area_categories = EducationCategory.objects.all()
        course_edu_effects = CourseLearningOutcome.objects.filter(course=course)
        course_effects = []
        
        for category in edu_area_categories:
            category_effects = []
            for effect in course_edu_effects:
                if effect.education_category == category:
                    category_effects.append(effect)
            course_effects.append({'category': category, 'effects': category_effects})
        return course_effects
            
    def _modules_effects(course):
        effects_map = {}
        for effect in CourseLearningOutcome.objects.filter(course=course):
            for module in effect.get_related_modules():
                if not module.id in effects_map:
                    effects_map[module.id] = {}
                if not effect.education_category.id in effects_map[module.id]: 
                    effects_map[module.id][effect.education_category.id] = []
                effects_map[module.id][effect.education_category.id].append(effect)
    
        edu_area_categories = EducationCategory.objects.all()
        
        module_effects_map = []
        for sgroup in course.sgroup_set.all():
            for module in sgroup.modules.all():
                module_effects = []
                for category in edu_area_categories:
                    category_effects = {'category': category, 'effects': []}
                    if module.id in effects_map and category.id in effects_map[module.id]:
                        category_effects['effects'] = effects_map[module.id][category.id]
                    module_effects.append(category_effects)
                         
                module_effects_map.append({'module': module, 'effects': module_effects})
        return module_effects_map
    
    try:
        course = Course.objects.active().get(id = course_id)
    except Course.DoesNotExist:
        messages.error(request, _(u'Internal error, please contact the Administrator.') + ' #ps0.')
        return redirect('apps.merovingian.views.profile_list')
    
    categories_list = EducationCategory.objects.all()
    
    course_effects = _course_effects(course)
    modules_effects = _modules_effects(course)
    
    try:
        degree_profile = CourseDegreeProfile.objects.get(course=course)
    except CourseDegreeProfile.DoesNotExist:
        degree_profile = CourseDegreeProfile()
        degree_profile.course = course
        degree_profile.save()
    
    # Has right to edit profile
    user_can_edit_profile = False
    if request.user.is_authenticated():
        if request.user.is_superuser:
            user_can_edit_profile = True
        else:
            try:
                MerovingianAdmin.objects.get(user_profile=request.user.get_profile(), courses=course)
                user_can_edit_profile = True 
            except MerovingianAdmin.DoesNotExist:
                user_can_edit_profile = False
    
    kwargs = { 
              'course': course, 
              'course_effects': course_effects, 
              'modules_effects': modules_effects, 
              'degree_profile': degree_profile,
              'categories_list': categories_list,
              'user_can_edit_profile': user_can_edit_profile,
              }
    
    return render_to_response('merovingian/profile/show.html',
                              kwargs,
                              context_instance = RequestContext(request))

@login_required
@permission_required('merovingian.change_coursedegreeprofile')
def edit(request, course_id):
    """
    """
    try:
        course = Course.objects.active().get(id = course_id)
    except Course.DoesNotExist:
        messages.error(request, _(u'Internal error, please contact the Administrator.') + ' #ps0.')
        return redirect('apps.merovingian.views.course.list')
    
    try:
        degree_profile = course.coursedegreeprofile
    except CourseDegreeProfile.DoesNotExist:
        degree_profile = CourseDegreeProfile()
        degree_profile.course = course
        degree_profile.save()
        
    form = DegreeProfileForm(instance=degree_profile)
    if request.method == "POST":
        form = DegreeProfileForm(instance=degree_profile, data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, _(u'Degree profile has been saved succesfully.'))
            return redirect('apps.merovingian.views.profile.edit', course_id = course.id)
        else:
            print form.errors
            messages.error(request, _(u'Correct errors listed below.'))
    
    kwargs = {'course': course, 'form': form}
    return render_to_response('merovingian/profile/edit.html',
                              kwargs,
                              context_instance = RequestContext(request))
    