# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.utils import translation
from django.db import transaction

from apps.merovingian.models import Course
from django.conf import settings
import syjon

class Command(BaseCommand):
    args = '<course_id>'
    help = "Refresh course assignation to didactic offer"
    
    def force_save_course(self, course):
        """
        Goes though all course descendants (subjects, modules) and saves them to update didactic offer.
        """
        course.save()
        for sgroup in course.sgroup_set.all():
            sgroup.save()
            for module in sgroup.modules.all():
                module.save()
                for subject in module.subject_set.all():
                    subject.save()

    def handle(self, *args, **options):
        
        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))
        
        if len(args) != 1:
            raise CommandError('Wrong number of parameters. Expected: ' + self.args)
        
        course = Course.objects.get(pk=int(args[0]))
        verbosity = options.get('verbosity',1)
        
        with transaction.commit_on_success():
            if verbosity > 1:
                print unicode(course)
            self.force_save_course(course)
        
        
        
        

