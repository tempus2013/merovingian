# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        # Note: Remember to use orm['appname.ModelName'] rather than "from appname.models..."
        
        ################################################
        #                 MAJOR LEVEL
        ################################################
        
        ml = orm['merovingian.CourseLevel']()
        ml.name = u'BA'
        ml.name_pl = u'I stopień'
        ml.name_en = u'BA'
        ml.save()
        
        ml = orm['merovingian.CourseLevel']()
        ml.name = u'MsC'
        ml.name_pl = u'II stopień'
        ml.name_en = u'MsC'
        ml.save()
        
        ml = orm['merovingian.CourseLevel']()
        ml.name = u'PhD'
        ml.name_pl = u'III stopień'
        ml.name_en = u'PhD'
        ml.save()
        
        ml = orm['merovingian.CourseLevel']()
        ml.name = u'uniform MsC'
        ml.name_pl = u'jednolite magisterskie'
        ml.name_en = u'uniform MsC'
        ml.save()
        
        ml = orm['merovingian.CourseLevel']()
        ml.name = u'engineering'
        ml.name_pl = u'inżynierskie'
        ml.name_en = u'engineering'
        ml.save()
        
        ml = orm['merovingian.CourseLevel']()
        ml.name = u'postgraduate qualifying'
        ml.name_pl = u'podyplomowe kwalifikacyjne'
        ml.name_en = u'postgraduate qualifying'
        ml.save()
        
        ml = orm['merovingian.CourseLevel']()
        ml.name = u'postgraduate training'
        ml.name_pl = u'podyplomowe dokształcające'
        ml.name_en = u'postgraduate training'
        ml.save()
        
        ################################################
        #                 MAJOR PROFILE
        ################################################
        
        mp = orm['merovingian.CourseProfile']()
        mp.name = u'practical'
        mp.name_pl = u'praktyczny'
        mp.name_en = u'practical'
        mp.save()
        
        mp = orm['merovingian.CourseProfile']()
        mp.name = u'university-wide'
        mp.name_pl = u'ogólnoakademicki'
        mp.name_en = u'university-wide'
        mp.save()
        
        ################################################
        #                 MAJOR TYPE
        ################################################
        
        obj = orm['merovingian.CourseType']()
        obj.name = u'full-time'
        obj.name_pl = u'stacjonarny'
        obj.name_en = u'full-time'
        obj.save()
        
        obj = orm['merovingian.CourseType']()
        obj.name = u'part-time'
        obj.name_pl = u'niestacjonarny'
        obj.name_en = u'part-time'
        obj.save()
        
        ################################################
        #                 MODULE TYPE
        ################################################
        
        obj = orm['merovingian.ModuleType']()
        obj.name = u'obligatory'
        obj.name_pl = u'obowiązkowy'
        obj.name_en = u'obligatory'
        obj.save()
        
        obj = orm['merovingian.ModuleType']()
        obj.name = u'elective'
        obj.name_pl = u'wybieralny'
        obj.name_en = u'elective'
        obj.save()
        
        obj = orm['merovingian.ModuleType']()
        obj.name = u'university-wide'
        obj.name_pl = u'ogólnoakademicki'
        obj.name_en = u'university-wide'
        obj.save()
        
        obj = orm['merovingian.ModuleType']()
        obj.name = u'specialty'
        obj.name_pl = u'specjalnościowy'
        obj.name_en = u'specialty'
        obj.save()
        
        obj = orm['merovingian.ModuleType']()
        obj.name = u'specialisation'
        obj.name_pl = u'specjalizacyjny'
        obj.name_en = u'specialisation'
        obj.save()
        
        obj = orm['merovingian.ModuleType']()
        obj.name = u'elective (facultative subjects)'
        obj.name_pl = u'wybieralny (zajęcia fakultatywne)'
        obj.name_en = u'elective (facultative subjects)'
        obj.save()
        
        obj = orm['merovingian.ModuleType']()
        obj.name = u'additional'
        obj.name_pl = u'dodatkowy'
        obj.name_en = u'additional'
        obj.save()
        
        ################################################
        #                 SGROUP TYPE
        ################################################
        
        obj = orm['merovingian.SGroupType']()
        obj.name = u'specialty'
        obj.name_pl = u'specjalność'
        obj.name_en = u'specialty'
        obj.save()
        
        obj = orm['merovingian.SGroupType']()
        obj.name = u'specialization'
        obj.name_pl = u'specjalizacja'
        obj.name_en = u'specialization'
        obj.save()
        
        obj = orm['merovingian.SGroupType']()
        obj.name = u'whole course'
        obj.name_pl = u'cały kierunek'
        obj.name_en = u'whole course'
        obj.save()
        
        ################################################
        #            SUBJECT ASSESSMENT
        ################################################
        
        obj = orm['merovingian.SubjectAssessment']()
        obj.name = u'exam'
        obj.name_pl = u'egzamin'
        obj.name_en = u'exam'
        obj.save()
        
        obj = orm['merovingian.SubjectAssessment']()
        obj.name = u'pass with grade'
        obj.name_pl = u'zaliczenie na ocenę'
        obj.name_en = u'pass with grade'
        obj.save()
        
        obj = orm['merovingian.SubjectAssessment']()
        obj.name = u'pass'
        obj.name_pl = u'zaliczenie'
        obj.name_en = u'pass'
        obj.save()
        
        ################################################
        #            SUBJECT TYPE
        ################################################
        
        obj = orm['merovingian.SubjectType']()
        obj.name = u'lecture'
        obj.name_pl = u'wykład'
        obj.name_en = u'lecture'
        obj.save()
        
        obj = orm['merovingian.SubjectType']()
        obj.name = u'laboratory'
        obj.name_pl = u'laboratorium'
        obj.name_en = u'laboratory'
        obj.save()
        
        obj = orm['merovingian.SubjectType']()
        obj.name = u'discussion seminar'
        obj.name_pl = u'konwersatorium'
        obj.name_en = u'discussion seminar'
        obj.save()
        
        obj = orm['merovingian.SubjectType']()
        obj.name = u'exercises'
        obj.name_pl = u'ćwiczenia'
        obj.name_en = u'exercises'
        obj.save()
        
        obj = orm['merovingian.SubjectType']()
        obj.name = u'seminar'
        obj.name_pl = u'seminarium'
        obj.name_en = u'seminar'
        obj.save()
        
        obj = orm['merovingian.SubjectType']()
        obj.name = u'practice'
        obj.name_pl = u'praktyki'
        obj.name_en = u'practice'
        obj.save()
        
        obj = orm['merovingian.SubjectType']()
        obj.name = u'field exercises'
        obj.name_pl = u'ćwiczenia terenowe'
        obj.name_en = u'field exercises'
        obj.save()
        
        obj = orm['merovingian.SubjectType']()
        obj.name = u'lectureship'
        obj.name_pl = u'lektorat'
        obj.name_en = u'lectureship'
        obj.save()

    def backwards(self, orm):
        "Write your backwards methods here."


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'megacity.sgrouptostudent': {
            'Meta': {'object_name': 'SGroupToStudent', 'db_table': "'megacity_sgroup__to__student'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_main': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_column': "'is_main'"}),
            'semester': ('django.db.models.fields.IntegerField', [], {'db_column': "'semester'"}),
            'sgroup': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SGroup']", 'db_column': "'id_sgroup'"}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Student']", 'db_column': "'id_student'"})
        },
        'merovingian.didacticoffer': {
            'Meta': {'ordering': "('-is_active', 'name')", 'object_name': 'DidacticOffer', 'db_table': "'merv_didactic_offer'"},
            'end_date': ('django.db.models.fields.DateField', [], {'db_column': "'end_date'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'db_column': "'start_date'"})
        },
        'merovingian.course': {
            'Meta': {'ordering': "('-is_active', 'name', '-level__name', '-type__name', 'profile__name', 'start_date')", 'object_name': 'Course', 'db_table': "'merv_course'"},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']", 'null': 'True', 'db_column': "'id_department'", 'blank': 'True'}),
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_first': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'db_column': "'is_first'", 'blank': 'True'}),
            'is_last': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'db_column': "'is_last'", 'blank': 'True'}),
            'level': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseLevel']", 'db_column': "'id_level'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseProfile']", 'null': 'True', 'db_column': "'id_profile'", 'blank': 'True'}),
            'semesters': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'semesters'"}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'start_date'", 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseType']", 'db_column': "'id_type'"})
        },
        'merovingian.courselevel': {
            'Meta': {'object_name': 'CourseLevel', 'db_table': "'merv_course_level'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.courseprofile': {
            'Meta': {'object_name': 'CourseProfile', 'db_table': "'merv_course_profile'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.coursetype': {
            'Meta': {'object_name': 'CourseType', 'db_table': "'merv_course_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.merovingianadmin': {
            'Meta': {'object_name': 'MerovingianAdmin', 'db_table': "'merv_admin'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Course']", 'null': 'True', 'db_table': "'merv_admin__to__course'", 'blank': 'True'}),
            'user_profile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['trainman.UserProfile']", 'unique': 'True', 'db_column': "'merv_admin__to__user_profile'"})
        },
        'merovingian.merovingiannews': {
            'Meta': {'ordering': "('-date', '-is_active')", 'object_name': 'MerovingianNews', 'db_table': "'merv_news'"},
            'date': ('django.db.models.fields.DateField', [], {'db_column': "'date'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'text_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'text_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'merovingian.merovingiansettings': {
            'Meta': {'object_name': 'MerovingianSettings', 'db_table': "'merv_settings'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32', 'db_column': "'key'"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'value_en': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'value_pl': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.mgroup': {
            'Meta': {'object_name': 'MGroup', 'db_table': "'merv_mgroup'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modules': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['merovingian.Module']", 'db_table': "'merv_mgroup__to__module'", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.MGroupType']", 'db_column': "'id_mgroup_type'"})
        },
        'merovingian.mgrouptype': {
            'Meta': {'ordering': "('name',)", 'object_name': 'MGroupType', 'db_table': "'merv_mgroup_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.module': {
            'Meta': {'ordering': "('-is_active', 'type', 'name')", 'object_name': 'Module', 'db_table': "'merv_module'"},
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'ects': ('django.db.models.fields.FloatField', [], {'null': 'True', 'db_column': "'ects'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.ModuleType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'merovingian.moduleproperties': {
            'Meta': {'ordering': "('semester',)", 'unique_together': "(('semester', 'module'),)", 'object_name': 'ModuleProperties', 'db_table': "'merv_module_properties'"},
            'ects': ('django.db.models.fields.FloatField', [], {'db_column': "'ects'"}),
            'hours': ('django.db.models.fields.FloatField', [], {'null': 'True', 'db_column': "'hours'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Module']", 'db_column': "'id_module'"}),
            'semester': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'semester'"}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SubjectType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'merovingian.moduletype': {
            'Meta': {'ordering': "('name',)", 'object_name': 'ModuleType', 'db_table': "'merv_module_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.sgroup': {
            'Meta': {'ordering': "('-is_active', '-type', 'name')", 'object_name': 'SGroup', 'db_table': "'merv_sgroup'"},
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Course']", 'db_column': "'id_course'"}),
            'modules': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Module']", 'null': 'True', 'db_table': "'merv_module__to__sgroup'", 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'sgroup': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'db_column': "'id_sgroup'", 'to': "orm['merovingian.SGroup']"}),
            'start_semester': ('django.db.models.fields.IntegerField', [], {'default': '1', 'max_length': '2', 'db_column': "'start_semester'"}),
            'students': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['trainman.Student']", 'null': 'True', 'through': "orm['megacity.SGroupToStudent']", 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SGroupType']", 'db_column': "'id_type'"})
        },
        'merovingian.sgrouptype': {
            'Meta': {'object_name': 'SGroupType', 'db_table': "'merv_sgroup_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.subject': {
            'Meta': {'ordering': "('semester', 'name', '-type', 'assessment')", 'object_name': 'Subject', 'db_table': "'merv_subject'"},
            'assessment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SubjectAssessment']", 'db_column': "'id_assessment'"}),
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'ects': ('django.db.models.fields.FloatField', [], {'null': 'True', 'db_column': "'ects'", 'blank': 'True'}),
            'hours': ('django.db.models.fields.FloatField', [], {'db_column': "'hours'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Module']", 'db_column': "'id_module'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'semester': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'semester'"}),
            'teachers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['trainman.Teacher']", 'null': 'True', 'through': "orm['merovingian.SubjectToTeacher']", 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SubjectType']", 'db_column': "'id_type'"})
        },
        'merovingian.subjectassessment': {
            'Meta': {'ordering': "('name',)", 'object_name': 'SubjectAssessment', 'db_table': "'merv_subject_assessment'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.subjecttoteacher': {
            'Meta': {'object_name': 'SubjectToTeacher', 'db_table': "'merv_subject__to__teacher'"},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'db_column': "'description'", 'blank': 'True'}),
            'groups': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'groups'"}),
            'hours': ('django.db.models.fields.FloatField', [], {'max_length': '3', 'db_column': "'hours'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Subject']", 'db_column': "'id_subject'"}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Teacher']", 'db_column': "'id_teacher'"})
        },
        'merovingian.subjecttype': {
            'Meta': {'ordering': "('name',)", 'object_name': 'SubjectType', 'db_table': "'merv_subject_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'trainman.department': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Department'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'db_column': "'id_department'", 'to': "orm['trainman.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_column': "'name'"}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.DepartmentType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'trainman.departmenttype': {
            'Meta': {'object_name': 'DepartmentType', 'db_table': "'trainman_department_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256', 'db_column': "'name'"})
        },
        'trainman.student': {
            'Meta': {'ordering': "('user_profile__user__last_name',)", 'object_name': 'Student'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'max_length': '16', 'db_column': "'number'"}),
            'user_profile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['trainman.UserProfile']", 'unique': 'True', 'db_column': "'user_profile'"})
        },
        'trainman.teacher': {
            'Meta': {'ordering': "('user_profile__user__last_name',)", 'object_name': 'Teacher'},
            'degree': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.TeacherDegree']", 'db_column': "'id_degree'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.TeacherPosition']", 'db_column': "'id_position'"}),
            'user_profile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['trainman.UserProfile']", 'unique': 'True', 'db_column': "'user_profile'"})
        },
        'trainman.teacherdegree': {
            'Meta': {'ordering': "('name',)", 'object_name': 'TeacherDegree', 'db_table': "'trainman_teacher_degree'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_column': "'name'"})
        },
        'trainman.teacherposition': {
            'Meta': {'ordering': "('name',)", 'object_name': 'TeacherPosition', 'db_table': "'trainman_teacher_position'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_column': "'name'"})
        },
        'trainman.userprofile': {
            'Meta': {'ordering': "('user__last_name',)", 'object_name': 'UserProfile', 'db_table': "'trainman_user_profile'"},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']", 'null': 'True', 'db_column': "'id_department'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pesel': ('django.db.models.fields.CharField', [], {'max_length': '11', 'null': 'True', 'db_column': "'pesel'", 'blank': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'db_column': "'second_name'", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'db_column': "'user'"})
        },
        'trinity.areaeducationeffect': {
            'Meta': {'ordering': "['symbol']", 'object_name': 'AreaEducationEffect', 'db_table': "'trinity_aee'"},
            'description': ('django.db.models.fields.TextField', [], {}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_area': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.EducationArea']", 'db_column': "'id_education_area'"}),
            'education_area_category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.EducationAreaCategory']", 'db_column': "'id_education_area_category'"}),
            'education_effect': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.EducationEffect']", 'db_column': "'id_education_effect'"}),
            'education_level': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseLevel']", 'db_column': "'id_education_level'"}),
            'education_profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseProfile']", 'db_column': "'id_education_profile'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'symbol_en': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_pl': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'})
        },
        'trinity.educationarea': {
            'Meta': {'ordering': "['name']", 'object_name': 'EducationArea', 'db_table': "'trinity_education_area'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Course']", 'null': 'True', 'db_table': "'trinity_course__to__education_area'", 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'trinity.educationareacategory': {
            'Meta': {'ordering': "['-name']", 'object_name': 'EducationAreaCategory', 'db_table': "'trinity_education_area_category'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'trinity.educationareaprofile': {
            'Meta': {'ordering': "['name']", 'object_name': 'EducationAreaProfile', 'db_table': "'trinity_education_area_profile'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'trinity.educationdiscipline': {
            'Meta': {'ordering': "['name']", 'object_name': 'EducationDiscipline', 'db_table': "'trinity_education_discipline'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Course']", 'null': 'True', 'db_table': "'trinity_course__to__education_discipline'", 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'trinity.educationeffect': {
            'Meta': {'ordering': "['begin_date', 'name']", 'object_name': 'EducationEffect', 'db_table': "'trinity_education_effect'"},
            'begin_date': ('django.db.models.fields.DateField', [], {}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'trinity.educationfield': {
            'Meta': {'ordering': "['area']", 'object_name': 'EducationField', 'db_table': "'trinity_education_field'"},
            'area': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.KnowledgeArea']", 'db_column': "'id_knowledge_area'"}),
            'discipline': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['trinity.EducationDiscipline']", 'null': 'True', 'db_table': "'trinity_education_field__to__education_discipline'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Course']", 'null': 'True', 'db_table': "'trinity_course__to__education_field'", 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'trinity.knowledgearea': {
            'Meta': {'ordering': "['name']", 'object_name': 'KnowledgeArea', 'db_table': "'trinity_knowledge_area'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Course']", 'null': 'True', 'db_table': "'trinity_course__to__knowledge_area'", 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'trinity.courseeducationeffect': {
            'Meta': {'ordering': "['symbol']", 'object_name': 'CourseEducationEffect', 'db_table': "'trinity_mee'"},
            'aees': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['trinity.AreaEducationEffect']", 'db_table': "'trinity_mee__to__aee'", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_area_category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.EducationAreaCategory']", 'db_column': "'id_education_area_category'"}),
            'education_effect': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.EducationEffect']", 'db_column': "'id_education_effect'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_realized': ('django.db.models.fields.NullBooleanField', [], {'default': 'False', 'null': 'True', 'blank': 'True'}),
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Course']", 'db_column': "'id_course'"}),
            'modules': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['merovingian.Module']", 'db_table': "'trinity_mee__to__module'", 'symmetrical': 'False'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'symbol_en': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_pl': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'})
        },
        'trinity.meecomment': {
            'Meta': {'ordering': "['update_date', 'mee']", 'object_name': 'MEEComment', 'db_table': "'trinity_mee_comment'"},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            'comment_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'comment_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mee': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.CourseEducationEffect']", 'db_column': "'id_course'"}),
            'update_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'null': 'True', 'blank': 'True'})
        },
        'trinity.moduleeducationeffect': {
            'Meta': {'ordering': "['symbol']", 'object_name': 'ModuleEducationEffect', 'db_table': "'trinity_xee'"},
            'description': ('django.db.models.fields.TextField', [], {}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'education_effect': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.EducationEffect']", 'db_column': "'id_education_effect'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mees': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['trinity.CourseEducationEffect']", 'db_table': "'trinity_xee__to__mee'", 'symmetrical': 'False'}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Module']", 'db_column': "'id_module'"}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'symbol_en': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'symbol_pl': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'})
        },
        'trinity.peemodule': {
            'Meta': {'object_name': 'PEEModule', 'db_table': "'trinity_pee__to_module'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.IntegerField', [], {'max_length': '1', 'db_column': "'level'"}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Module']", 'db_column': "'id_module'"}),
            'pee': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.ProgramEducationEffect']", 'db_column': "'id_pee'"})
        },
        'trinity.programeducationeffect': {
            'Meta': {'object_name': 'ProgramEducationEffect', 'db_table': "'trinity_pee'"},
            'description': ('django.db.models.fields.TextField', [], {}),
            'education_effect': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trinity.EducationEffect']", 'db_column': "'id_education_effect'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mees': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['trinity.CourseEducationEffect']", 'db_table': "'trinity_pee__to__mee'", 'symmetrical': 'False'}),
            'modules': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['merovingian.Module']", 'through': "orm['trinity.PEEModule']", 'symmetrical': 'False'}),
            'sgroup': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SGroup']", 'null': 'True', 'db_column': "'id_sgroup'", 'blank': 'True'})
        },
        'trinity.trinitynews': {
            'Meta': {'ordering': "('-date', '-is_active')", 'object_name': 'TrinityNews', 'db_table': "'trinity_news'"},
            'content': ('django.db.models.fields.TextField', [], {'db_column': "'content'"}),
            'date': ('django.db.models.fields.DateField', [], {'db_column': "'date'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'trinity.trinityprofile': {
            'Meta': {'object_name': 'TrinityProfile', 'db_table': "'trinity_profile'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Course']", 'null': 'True', 'db_table': "'trinity_profile__to__course'", 'blank': 'True'}),
            'user_profile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['trainman.UserProfile']", 'unique': 'True', 'db_column': "'user_profile'"})
        }
    }

    complete_apps = ['trinity', 'merovingian']
    symmetrical = True