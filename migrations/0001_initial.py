# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MerovingianSettings'
        db.create_table('merv_settings', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32, db_column='key')),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('value_pl', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('value_en', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('value_ua', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('value_ru', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
        ))
        db.send_create_signal('merovingian', ['MerovingianSettings'])

        # Adding model 'MerovingianAdmin'
        db.create_table('merv_admin', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user_profile', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['trainman.UserProfile'], unique=True, db_column='merv_admin__to__user_profile')),
            ('temporary_privileged_access', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('merovingian', ['MerovingianAdmin'])

        # Adding M2M table for field courses on 'MerovingianAdmin'
        db.create_table('merv_admin__to__course', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('merovingianadmin', models.ForeignKey(orm['merovingian.merovingianadmin'], null=False)),
            ('course', models.ForeignKey(orm['merovingian.course'], null=False))
        ))
        db.create_unique('merv_admin__to__course', ['merovingianadmin_id', 'course_id'])

        # Adding model 'MerovingianNews'
        db.create_table('merv_news', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('date', self.gf('django.db.models.fields.DateField')(db_column='date')),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('text_pl', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('text_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('text_ua', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('text_ru', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('merovingian', ['MerovingianNews'])

        # Adding model 'DidacticOffer'
        db.create_table('merv_didactic_offer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('start_date', self.gf('django.db.models.fields.DateField')(db_column='start_date')),
            ('end_date', self.gf('django.db.models.fields.DateField')(db_column='end_date')),
        ))
        db.send_create_signal('merovingian', ['DidacticOffer'])

        # Adding model 'CourseLevel'
        db.create_table('merv_course_level', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
        ))
        db.send_create_signal('merovingian', ['CourseLevel'])

        # Adding model 'CourseType'
        db.create_table('merv_course_type', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
        ))
        db.send_create_signal('merovingian', ['CourseType'])

        # Adding model 'CourseProfile'
        db.create_table('merv_course_profile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
        ))
        db.send_create_signal('merovingian', ['CourseProfile'])

        # Adding model 'Course'
        db.create_table('merv_course', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('didactic_offer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.DidacticOffer'], null=True, db_column='id_didactic_offer', blank=True)),
            ('semesters', self.gf('django.db.models.fields.IntegerField')(max_length=2, null=True, db_column='semesters', blank=True)),
            ('years', self.gf('django.db.models.fields.IntegerField')(max_length=2, null=True, db_column='years', blank=True)),
            ('start_date', self.gf('django.db.models.fields.DateField')(null=True, db_column='start_date', blank=True)),
            ('end_date', self.gf('django.db.models.fields.DateField')(null=True, db_column='end_date', blank=True)),
            ('level', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.CourseLevel'], db_column='id_level')),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.CourseType'], db_column='id_type')),
            ('profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.CourseProfile'], null=True, db_column='id_profile', blank=True)),
            ('department', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.Department'], null=True, db_column='id_department', blank=True)),
            ('is_first', self.gf('django.db.models.fields.NullBooleanField')(default=None, null=True, db_column='is_first', blank=True)),
            ('is_last', self.gf('django.db.models.fields.NullBooleanField')(default=None, null=True, db_column='is_last', blank=True)),
        ))
        db.send_create_signal('merovingian', ['Course'])

        # Adding model 'SGroupType'
        db.create_table('merv_sgroup_type', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
        ))
        db.send_create_signal('merovingian', ['SGroupType'])

        # Adding model 'SGroup'
        db.create_table('merv_sgroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('didactic_offer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.DidacticOffer'], null=True, db_column='id_didactic_offer', blank=True)),
            ('start_semester', self.gf('django.db.models.fields.IntegerField')(default=1, max_length=2, db_column='start_semester')),
            ('sgroup', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, db_column='id_sgroup', to=orm['merovingian.SGroup'])),
            ('course', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.Course'], db_column='id_course')),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.SGroupType'], db_column='id_type')),
        ))
        db.send_create_signal('merovingian', ['SGroup'])

        # Adding M2M table for field modules on 'SGroup'
        db.create_table('merv_module__to__sgroup', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('sgroup', models.ForeignKey(orm['merovingian.sgroup'], null=False)),
            ('module', models.ForeignKey(orm['merovingian.module'], null=False))
        ))
        db.create_unique('merv_module__to__sgroup', ['sgroup_id', 'module_id'])

        # Adding model 'ModuleType'
        db.create_table('merv_module_type', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
        ))
        db.send_create_signal('merovingian', ['ModuleType'])

        # Adding model 'Module'
        db.create_table('merv_module', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('didactic_offer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.DidacticOffer'], null=True, db_column='id_didactic_offer', blank=True)),
            ('ects', self.gf('django.db.models.fields.FloatField')(null=True, db_column='ects', blank=True)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.ModuleType'], null=True, db_column='id_type', blank=True)),
        ))
        db.send_create_signal('merovingian', ['Module'])

        # Adding model 'ModuleProperties'
        db.create_table('merv_module_properties', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('semester', self.gf('django.db.models.fields.IntegerField')(max_length=2, db_column='semester')),
            ('hours', self.gf('django.db.models.fields.FloatField')(null=True, db_column='hours', blank=True)),
            ('ects', self.gf('django.db.models.fields.FloatField')(db_column='ects')),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.SubjectType'], null=True, db_column='id_type', blank=True)),
            ('module', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.Module'], db_column='id_module')),
        ))
        db.send_create_signal('merovingian', ['ModuleProperties'])

        # Adding unique constraint on 'ModuleProperties', fields ['semester', 'module', 'type']
        db.create_unique('merv_module_properties', ['semester', 'id_module', 'id_type'])

        # Adding model 'SubjectType'
        db.create_table('merv_subject_type', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
        ))
        db.send_create_signal('merovingian', ['SubjectType'])

        # Adding model 'SubjectAssessment'
        db.create_table('merv_subject_assessment', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
        ))
        db.send_create_signal('merovingian', ['SubjectAssessment'])

        # Adding model 'Subject'
        db.create_table('merv_subject', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('didactic_offer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.DidacticOffer'], null=True, db_column='id_didactic_offer', blank=True)),
            ('hours', self.gf('django.db.models.fields.FloatField')(db_column='hours')),
            ('semester', self.gf('django.db.models.fields.IntegerField')(max_length=2, db_column='semester')),
            ('ects', self.gf('django.db.models.fields.FloatField')(null=True, db_column='ects', blank=True)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.SubjectType'], db_column='id_type')),
            ('assessment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.SubjectAssessment'], db_column='id_assessment')),
            ('module', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.Module'], db_column='id_module')),
        ))
        db.send_create_signal('merovingian', ['Subject'])

        # Adding model 'SubjectToTeacher'
        db.create_table('merv_subject__to__teacher', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('groups', self.gf('django.db.models.fields.IntegerField')(max_length=2, db_column='groups')),
            ('hours', self.gf('django.db.models.fields.FloatField')(max_length=3, db_column='hours')),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=512, null=True, db_column='description', blank=True)),
            ('teacher', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.Teacher'], db_column='id_teacher')),
            ('subject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.Subject'], db_column='id_subject')),
        ))
        db.send_create_signal('merovingian', ['SubjectToTeacher'])

        # Adding model 'CourseDegreeProfile'
        db.create_table('merv_degree_profile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('course', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['merovingian.Course'], unique=True)),
            ('type_length', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('institutions', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('accreditation_organisations', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('disciplines', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('purpose', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('focus', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('orientation', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('distinctive_features', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('employability', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('further_studies', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('teaching_approaches', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('assessment_methods', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
        ))
        db.send_create_signal('merovingian', ['CourseDegreeProfile'])

        # Adding model 'CourseDiagnosticInfo'
        db.create_table('merv_diagnostic_info', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('course', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['merovingian.Course'], unique=True)),
            ('minimal_ects_per_semester', self.gf('django.db.models.fields.IntegerField')()),
            ('maximal_hours', self.gf('django.db.models.fields.IntegerField')()),
            ('maximal_exams_per_semester', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('merovingian', ['CourseDiagnosticInfo'])


    def backwards(self, orm):
        # Removing unique constraint on 'ModuleProperties', fields ['semester', 'module', 'type']
        db.delete_unique('merv_module_properties', ['semester', 'id_module', 'id_type'])

        # Deleting model 'MerovingianSettings'
        db.delete_table('merv_settings')

        # Deleting model 'MerovingianAdmin'
        db.delete_table('merv_admin')

        # Removing M2M table for field courses on 'MerovingianAdmin'
        db.delete_table('merv_admin__to__course')

        # Deleting model 'MerovingianNews'
        db.delete_table('merv_news')

        # Deleting model 'DidacticOffer'
        db.delete_table('merv_didactic_offer')

        # Deleting model 'CourseLevel'
        db.delete_table('merv_course_level')

        # Deleting model 'CourseType'
        db.delete_table('merv_course_type')

        # Deleting model 'CourseProfile'
        db.delete_table('merv_course_profile')

        # Deleting model 'Course'
        db.delete_table('merv_course')

        # Deleting model 'SGroupType'
        db.delete_table('merv_sgroup_type')

        # Deleting model 'SGroup'
        db.delete_table('merv_sgroup')

        # Removing M2M table for field modules on 'SGroup'
        db.delete_table('merv_module__to__sgroup')

        # Deleting model 'ModuleType'
        db.delete_table('merv_module_type')

        # Deleting model 'Module'
        db.delete_table('merv_module')

        # Deleting model 'ModuleProperties'
        db.delete_table('merv_module_properties')

        # Deleting model 'SubjectType'
        db.delete_table('merv_subject_type')

        # Deleting model 'SubjectAssessment'
        db.delete_table('merv_subject_assessment')

        # Deleting model 'Subject'
        db.delete_table('merv_subject')

        # Deleting model 'SubjectToTeacher'
        db.delete_table('merv_subject__to__teacher')

        # Deleting model 'CourseDegreeProfile'
        db.delete_table('merv_degree_profile')

        # Deleting model 'CourseDiagnosticInfo'
        db.delete_table('merv_diagnostic_info')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'merovingian.course': {
            'Meta': {'ordering': "('-is_active', 'name', '-level__name', '-type__name', 'profile__name', 'start_date')", 'object_name': 'Course', 'db_table': "'merv_course'"},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']", 'null': 'True', 'db_column': "'id_department'", 'blank': 'True'}),
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'end_date'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_first': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'db_column': "'is_first'", 'blank': 'True'}),
            'is_last': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'db_column': "'is_last'", 'blank': 'True'}),
            'level': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseLevel']", 'db_column': "'id_level'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseProfile']", 'null': 'True', 'db_column': "'id_profile'", 'blank': 'True'}),
            'semesters': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'null': 'True', 'db_column': "'semesters'", 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'start_date'", 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseType']", 'db_column': "'id_type'"}),
            'years': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'null': 'True', 'db_column': "'years'", 'blank': 'True'})
        },
        'merovingian.coursedegreeprofile': {
            'Meta': {'object_name': 'CourseDegreeProfile', 'db_table': "'merv_degree_profile'"},
            'accreditation_organisations': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'assessment_methods': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'course': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['merovingian.Course']", 'unique': 'True'}),
            'disciplines': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'distinctive_features': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'employability': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'focus': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'further_studies': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institutions': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'orientation': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'purpose': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'teaching_approaches': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'type_length': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'})
        },
        'merovingian.coursediagnosticinfo': {
            'Meta': {'object_name': 'CourseDiagnosticInfo', 'db_table': "'merv_diagnostic_info'"},
            'course': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['merovingian.Course']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maximal_exams_per_semester': ('django.db.models.fields.IntegerField', [], {}),
            'maximal_hours': ('django.db.models.fields.IntegerField', [], {}),
            'minimal_ects_per_semester': ('django.db.models.fields.IntegerField', [], {})
        },
        'merovingian.courselevel': {
            'Meta': {'object_name': 'CourseLevel', 'db_table': "'merv_course_level'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.courseprofile': {
            'Meta': {'object_name': 'CourseProfile', 'db_table': "'merv_course_profile'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.coursetype': {
            'Meta': {'object_name': 'CourseType', 'db_table': "'merv_course_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.didacticoffer': {
            'Meta': {'object_name': 'DidacticOffer', 'db_table': "'merv_didactic_offer'"},
            'end_date': ('django.db.models.fields.DateField', [], {'db_column': "'end_date'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'db_column': "'start_date'"})
        },
        'merovingian.merovingianadmin': {
            'Meta': {'object_name': 'MerovingianAdmin', 'db_table': "'merv_admin'"},
            'courses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Course']", 'null': 'True', 'db_table': "'merv_admin__to__course'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'temporary_privileged_access': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user_profile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['trainman.UserProfile']", 'unique': 'True', 'db_column': "'merv_admin__to__user_profile'"})
        },
        'merovingian.merovingiannews': {
            'Meta': {'ordering': "('-date', '-is_active')", 'object_name': 'MerovingianNews', 'db_table': "'merv_news'"},
            'date': ('django.db.models.fields.DateField', [], {'db_column': "'date'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'text_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'text_pl': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'text_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'text_ua': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'merovingian.merovingiansettings': {
            'Meta': {'object_name': 'MerovingianSettings', 'db_table': "'merv_settings'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32', 'db_column': "'key'"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'value_en': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'value_pl': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'value_ru': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'value_ua': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.module': {
            'Meta': {'ordering': "['name']", 'object_name': 'Module', 'db_table': "'merv_module'"},
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'ects': ('django.db.models.fields.FloatField', [], {'null': 'True', 'db_column': "'ects'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.ModuleType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'merovingian.moduleproperties': {
            'Meta': {'ordering': "('semester',)", 'unique_together': "(('semester', 'module', 'type'),)", 'object_name': 'ModuleProperties', 'db_table': "'merv_module_properties'"},
            'ects': ('django.db.models.fields.FloatField', [], {'db_column': "'ects'"}),
            'hours': ('django.db.models.fields.FloatField', [], {'null': 'True', 'db_column': "'hours'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Module']", 'db_column': "'id_module'"}),
            'semester': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'semester'"}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SubjectType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'merovingian.moduletype': {
            'Meta': {'ordering': "('name',)", 'object_name': 'ModuleType', 'db_table': "'merv_module_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.sgroup': {
            'Meta': {'ordering': "('-is_active', '-type', 'name')", 'object_name': 'SGroup', 'db_table': "'merv_sgroup'"},
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Course']", 'db_column': "'id_course'"}),
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'modules': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['merovingian.Module']", 'null': 'True', 'db_table': "'merv_module__to__sgroup'", 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'sgroup': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'db_column': "'id_sgroup'", 'to': "orm['merovingian.SGroup']"}),
            'start_semester': ('django.db.models.fields.IntegerField', [], {'default': '1', 'max_length': '2', 'db_column': "'start_semester'"}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SGroupType']", 'db_column': "'id_type'"})
        },
        'merovingian.sgrouptype': {
            'Meta': {'object_name': 'SGroupType', 'db_table': "'merv_sgroup_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.subject': {
            'Meta': {'ordering': "('semester', 'name', '-type', 'assessment')", 'object_name': 'Subject', 'db_table': "'merv_subject'"},
            'assessment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SubjectAssessment']", 'db_column': "'id_assessment'"}),
            'didactic_offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.DidacticOffer']", 'null': 'True', 'db_column': "'id_didactic_offer'", 'blank': 'True'}),
            'ects': ('django.db.models.fields.FloatField', [], {'null': 'True', 'db_column': "'ects'", 'blank': 'True'}),
            'hours': ('django.db.models.fields.FloatField', [], {'db_column': "'hours'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Module']", 'db_column': "'id_module'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'semester': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'semester'"}),
            'teachers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['trainman.Teacher']", 'null': 'True', 'through': "orm['merovingian.SubjectToTeacher']", 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.SubjectType']", 'db_column': "'id_type'"})
        },
        'merovingian.subjectassessment': {
            'Meta': {'ordering': "('name',)", 'object_name': 'SubjectAssessment', 'db_table': "'merv_subject_assessment'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.subjecttoteacher': {
            'Meta': {'object_name': 'SubjectToTeacher', 'db_table': "'merv_subject__to__teacher'"},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'db_column': "'description'", 'blank': 'True'}),
            'groups': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'db_column': "'groups'"}),
            'hours': ('django.db.models.fields.FloatField', [], {'max_length': '3', 'db_column': "'hours'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.Subject']", 'db_column': "'id_subject'"}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Teacher']", 'db_column': "'id_teacher'"})
        },
        'merovingian.subjecttype': {
            'Meta': {'ordering': "('name',)", 'object_name': 'SubjectType', 'db_table': "'merv_subject_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'trainman.department': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Department'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'db_column': "'id_department'", 'to': "orm['trainman.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.DepartmentType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'trainman.departmenttype': {
            'Meta': {'object_name': 'DepartmentType', 'db_table': "'trainman_department_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'trainman.teacher': {
            'Meta': {'ordering': "('user_profile__user__last_name',)", 'object_name': 'Teacher'},
            'degree': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.TeacherDegree']", 'db_column': "'id_degree'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.TeacherPosition']", 'db_column': "'id_position'"}),
            'user_profile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['trainman.UserProfile']", 'unique': 'True', 'db_column': "'user_profile'"})
        },
        'trainman.teacherdegree': {
            'Meta': {'ordering': "('name',)", 'object_name': 'TeacherDegree', 'db_table': "'trainman_teacher_degree'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'trainman.teacherposition': {
            'Meta': {'ordering': "('name',)", 'object_name': 'TeacherPosition', 'db_table': "'trainman_teacher_position'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'trainman.userprofile': {
            'Meta': {'ordering': "('user__last_name',)", 'object_name': 'UserProfile', 'db_table': "'trainman_user_profile'"},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']", 'null': 'True', 'db_column': "'id_department'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pesel': ('django.db.models.fields.CharField', [], {'max_length': '11', 'null': 'True', 'db_column': "'pesel'", 'blank': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'db_column': "'second_name'", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'db_column': "'user'"})
        }
    }

    complete_apps = ['merovingian']