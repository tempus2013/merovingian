# -*- coding: utf-8 -*-

from django.contrib import admin
from django.utils.translation import ugettext as _

from apps.merovingian.models import MerovingianSettings, CourseDegreeProfile
from apps.merovingian.models import MerovingianAdmin
from apps.merovingian.models import MerovingianNews
from apps.merovingian.models import DidacticOffer
from apps.merovingian.models import CourseLevel
from apps.merovingian.models import CourseType
from apps.merovingian.models import CourseProfile
from apps.merovingian.models import Course
from apps.merovingian.models import SGroupType
from apps.merovingian.models import SGroup
from apps.merovingian.models import ModuleType
from apps.merovingian.models import Module
from apps.merovingian.models import ModuleProperties
from apps.merovingian.models import SubjectType
from apps.merovingian.models import SubjectAssessment
from apps.merovingian.models import Subject

class MerovingianAdminAdmin(admin.ModelAdmin):
    list_display = ('user_profile', 'get_email', 'get_courses')
    search_fields = ('^user_profile__user__last_name', '^user_profile__user__username')
    ordering = ('user_profile__user__last_name',)

    filter_vertical = ('courses',)

    def get_email(self, obj):
        return obj.user_profile.user.email
    get_email.short_description = _(u'email')

    def get_courses(self, obj):
        result = ''
        for m in obj.courses.all():
            result += unicode(m) + '; '
        return result
    get_courses.short_description = _(u'courses')

class CourseAdmin(admin.ModelAdmin):
    list_display = ('name', 'start_date', 'level', 'type', 'profile', 'is_active')
    search_fields = ('^name',)
    ordering = ('name', '-level__name', '-type__name')

class SGroupAdmin(admin.ModelAdmin):
    fields = ('is_active', 'name', 'type', 'course', 'sgroup', 'start_semester', 'didactic_offer', 'modules')
    list_display = ('course', 'name', 'type')
    search_fields = ('^course__name', '^name')
    ordering = ('course__name', )

    filter_vertical = ('modules',)

class ModuleAdmin(admin.ModelAdmin):
    search_fields = ('^name',)
    list_display = ('name', 'get_sgroup', 'get_course')
    
    def get_course(self, obj):
        return obj.get_courses()[0]
    get_course.short_description = _(u'Course')
    
    def get_sgroup(self, obj):
        return obj.get_sgroups()[0].name
    get_sgroup.short_description = _(u'Specialty')

admin.site.register(MerovingianSettings)
admin.site.register(MerovingianAdmin, MerovingianAdminAdmin)
admin.site.register(MerovingianNews)
admin.site.register(DidacticOffer)
admin.site.register(CourseLevel)
admin.site.register(CourseType)
admin.site.register(CourseProfile)
admin.site.register(Course, CourseAdmin)
admin.site.register(CourseDegreeProfile)
admin.site.register(SGroupType)
admin.site.register(SGroup, SGroupAdmin)
admin.site.register(SubjectType)
admin.site.register(SubjectAssessment)
admin.site.register(ModuleType)
admin.site.register(Module, ModuleAdmin)
admin.site.register(ModuleProperties)
admin.site.register(Subject)
