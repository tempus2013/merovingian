# -*- coding: utf-8 -*-

from django import forms
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.forms.util import ErrorList

from apps.trainman.models import Teacher

from apps.merovingian.models import Course, CourseDegreeProfile,\
    CourseDiagnosticInfo
from apps.merovingian.models import SGroup
from apps.merovingian.models import SGroupType
from apps.merovingian.models import Module
from apps.merovingian.models import ModuleProperties
from apps.merovingian.models import Subject
from apps.merovingian.models import DidacticOffer
from apps.trainman.models import Department, DEPARTMENT_TYPE_FACULTY

from apps.merovingian.functions import default_sgroup_name
from apps.merovingian.translation import TranslatedModelForm,\
    TranslatedInlineFormset

# --- Search ---

class SearchForm(forms.Form):
    name = forms.CharField(label = '',
                           required = False)

class SearchFilterForm(forms.Form):
    name = forms.CharField(label='', required=False)
    faculty = forms.ModelChoiceField(widget=forms.Select(), queryset=Department.objects.filter(type_id=DEPARTMENT_TYPE_FACULTY), required=False, empty_label=_(u"All departments"))

# --- Courses ---

class CourseForm(TranslatedModelForm):
    class Meta:
        model = Course
        
    def __init__(self, *args, **kwargs):
        super(CourseForm, self).__init__(*args, **kwargs)
        self.fields['start_date'].widget = forms.DateInput()
        
    def clean(self):
        cleaned_data = super(CourseForm,self).clean()
        if cleaned_data['years'] and cleaned_data['semesters']:
            errors = self._errors.setdefault('semesters', ErrorList())
            errors.append(_(u'Course cannot be settled per year and per semester at the same time. Fill in one field, either number of years ot number of semesters.'))
        return cleaned_data
        
class SGroupBaseInlineFormSet(TranslatedInlineFormset):
    def add_fields(self, form, index):
        super(SGroupBaseInlineFormSet, self).add_fields(form, index)
        form.fields['type'] = forms.ModelChoiceField(queryset = SGroupType.objects.exclude(name = default_sgroup_name()))
    def get_queryset(self):
        return super(SGroupBaseInlineFormSet, self).get_queryset().exclude(name = default_sgroup_name())
        
SGroupInlineFormset = forms.models.inlineformset_factory(Course, SGroup,  exclude = ('modules', 'students'), formset = SGroupBaseInlineFormSet)

# --- Modules ---

class LabelHiddenInput(forms.widgets.HiddenInput):
    def render(self, name, value, attrs = None):
        widget = super(LabelHiddenInput, self).render(name, value, attrs)
        label = unicode(Teacher.objects.get(id = int(value))) if value else u''
        return mark_safe(widget + u'<strong id="id_%s-label">' % (name) + label + u'</strong>')

class ModuleForm(TranslatedModelForm):
    class Meta:
        model = Module
        exclude = ('subjects',)

class ModulePropertiesFormSet(forms.models.BaseInlineFormSet):
    def get_queryset(self):
        return super(ModulePropertiesFormSet, self).get_queryset()

class SubjectBaseFormSet(TranslatedInlineFormset):
    def get_queryset(self):
        return super(SubjectBaseFormSet, self).get_queryset()

ModulePropertiesInlineFormset = forms.models.inlineformset_factory(Module, ModuleProperties, formset = ModulePropertiesFormSet, extra = 2, max_num = 10)

SubjectInlineFormset = forms.models.inlineformset_factory(Module, Subject, exclude = ('teachers',), formset = SubjectBaseFormSet, extra = 2)

# --- didactic offer ---

class DidacticOfferForm(TranslatedModelForm):
    class Meta:
        model = DidacticOffer
        
    def __init__(self, *args, **kwargs):
        super(DidacticOfferForm, self).__init__(*args, **kwargs)
        self.fields['start_date'].widget = forms.DateInput()
        self.fields['end_date'].widget = forms.DateInput()
        self.fields['start_date'].required = True
        self.fields['end_date'].required = True
        
    def clean(self):
        cleaned_data = super(DidacticOfferForm,self).clean()
    
        start_date = cleaned_data.get("start_date")
        end_date = cleaned_data.get("end_date")
    
        if start_date and end_date:
            if start_date > end_date:
                errors = self._errors.setdefault('start_date', ErrorList())
                errors.append(_(u'Start date cannot be later than end date')) 
            else:
                qs = DidacticOffer.objects
                if self.instance and self.instance.pk:
                    qs = qs.exclude(pk=self.instance.pk)
                doffers = qs.all()
                for doffer in doffers:
                    if start_date >= doffer.start_date and start_date <= doffer.end_date \
                        or end_date >= doffer.start_date and end_date <= doffer.end_date \
                        or doffer.start_date >= start_date and doffer.start_date <= end_date \
                        or doffer.end_date >= start_date and doffer.end_date <= end_date:
                        errors = self._errors.setdefault('start_date', ErrorList())
                        errors.append(_(u'The time period of teaching offer cannot overlap other offers')) 
                        break
        
        return cleaned_data
        
        
# --- Degree profile

class DegreeProfileForm(TranslatedModelForm):
    class Meta:
        model = CourseDegreeProfile
        
    def __init__(self, *args, **kwargs):
        super(DegreeProfileForm, self).__init__(*args, **kwargs)
        self.fields['course'].widget = forms.HiddenInput()
        
        
# --- Diagnostic Info

class DiagnosticInfoForm(forms.ModelForm):
    
    class Meta:
        model = CourseDiagnosticInfo
        
    def __init__(self, *args, **kwargs):
        super(DiagnosticInfoForm, self).__init__(*args, **kwargs)
        self.fields['course'].widget = forms.HiddenInput()
