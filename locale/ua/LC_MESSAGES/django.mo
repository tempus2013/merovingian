��    Z      �     �      �     �     �     �     �          
          *     /     ;     K     \     n     �     �     �     �  1   �     �     �     	     	  (   	     E	     W	     c	     p	     	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     ,
     5
     Q
     l
  ,   s
  Y   �
  \   �
     W  	   c      m  
   �     �     �     �     �     �     �     �     �     �               1     G     ^     n     t     x     }     �     �     �     �     �     �     �     �     �     �          	            
        &  
   3     >     F  e  L     �  $   �  =   �  *   &     Q     b     k          �     �     �  #   �  !   �          .     L     Y  k   u     �  -   �  +         L  /   Y     �     �     �     �     �  
   �       
   $     /     J     c     t     y     �  %   �  "   �     �  ,        H  <   [  E   �  
   �  P   �  �   :  �   �     �     �  H   �     +     C  #   R     v  %   �     �     �     �            '   +  >   S  4   �  4   �  )   �  
   &     1     8     @     X  $   p     �     �  &   �     �     �       
        "  !   +     M     k  
   t  0     .   �     �     �          5          /       9   ;   W   V                          G   H   "   1   ,           +   X   7       M   U              A       Y   #   O          3      
                    F         ?   T                  C   )      @   >      Q           D          .   B   6          0       J                                    '   	      :   $      *       !          &   =          R   S   4   L   <      -   Z   N      E   I       2   (             P      %       K      8            Active Add new module Add new teaching offer Correct errors listed below. Delete Description E<br/>C<br/>T<br/>S ECTS ECTS points Education level Education levels Education profile Education profiles Elective modules End date First First semester/year Internal error, please contact the Administrator. Last Merowing Administrator Merowing Settings Module Module %s has been deleted successfully. Module attributes Module type Module types Module/Subject Modules Name Name, active Names Names, active News Next No Number of groups Number of hours Number of semesters Obligatory modules P<br/>A<br/>S<br/>S Parent subjects group Previous Save and return to the list Save and stay on this page Search Selected item will be removed. Are you sure? Selected module does not exist. If you see this message again, contact the Administrator. Selected specialty does not exist. If you see this message again, contact the Administrator. Specialties Specialty Specialty/Specialization modules Start date Subject Subject - Teacher Subjects Subjects - Teachers Subjects group Subjects groups Sylabus Teacher Teachers Teaching offer Teaching offer attributes Teaching offer record Teaching offer records Teaching offers Total Typ Type Type of classes Type of pass Type of subjects group Types of classes Types of pass Types of subjects group Unit User Yes content date email end date key name no sylabus show sylabus start date teacher value Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-03-31 15:14+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Translate Toolkit 1.9.0
 Активний Додати новий модуль Додати нову дидатичну пропозицію Виправте помилки нижче Видалити Опис E<br/>C<br/>T<br/>S ECTS Пункти ECTS Рівень освіти Рівні освіти Навчальний профіль Навчальні профілі Вибіркові модулі дата закінчення Перший Перший семестр Помилка системи, будь ласка, зв'яжіться з Адміністратором.  Останній Адміністратор Меровінга Налаштування Меровінга Модуль Модуль %s успішно видалено Параметри модуля Тип модуля Типи модуля Модуль/предмет Модулі Назва Назва, активний Назви Назви, активні Актуальність Наступна Ні Кількість груп Кількість годин Кількість семестрів Обов'язкові модулі З<br/>А<br/>Р<br/>А<br/>Х Головна група предметів Попередня Зберегти і повернутися до списку Зберегти і залишитися на цій сторінці Пошук Вибраний елемент буде видалено. Продовжити? Вибраний модуль не існує. Якщо це повідомлення буде повторюватися, зв'яжіться з Адміністратором.  Вибрана спеціальність не існує. Якщо це повідомлення буде повторюватися, зв'яжіться з Адміністратором.  Спеціальності Спеціальність Спеціалістичні/спеціалізаційні модулі дата початку Предмет Предмет — викладач Предмети Предмети - викладачі Група предметів Групи предметів Навчальний план Викладач Викладачі Навчальна пропозиція Параметри дидактичної пропозиції Запис навчальної пропозиції Записи начальної пропозиції Дидактичні пропозиції Разом Тип Тип  Форма занять Форма заліку Тип групи предметів Форми занять Форми заліку Типи групи предметів Одиниця Користувач Так Зміст Дата електронна адреса дата закінчення ключ Назва Навчальний план відсутній Показати навчальний план дата початку Викладач вартість 