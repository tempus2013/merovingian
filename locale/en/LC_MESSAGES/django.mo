��          �      �       0     1  !   J  !   l  4   �     �  +   �       \   !     ~  	   �      �  S   �  D  	     N  !   g  %   �  8   �     �  2        ;  a   T     �     �     �  S   �                          	           
                     Course learning outcomes Modify modules (facultative only) Modify modules of all specialties Modify modules of all specialties (facultative only) Modify specialty modules Modify specialty modules (facultative only) Module learning outcomes Selected specialty does not exist. If you see this message again, contact the Administrator. Specialties Specialty Specialty/Specialization modules You cannot modify modules that belongs to course which is in active teaching offer. Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-03-31 15:14+0200
PO-Revision-Date: 2013-07-17 16:39+0100
Last-Translator: Damian Rusinek <damian.rusinek@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Course learning outcomes Modify modules (facultative only) Modify modules of all specializations Modify modules of all specializations (facultative only) Modify specialization's modules Modify specialization's modules (facultative only) Module learning outcomes Selected specialization does not exist. If you see this message again, contact the Administrator. Specializations Specialization Specialization modules You cannot modify modules that belongs to course which is in active teaching offer. 